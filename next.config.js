/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	// output: 'export',

	// Optional: Change the output directory `out` -> `dist`
	// distDir: 'dist',
};

module.exports = nextConfig;

FROM node:16-alpine AS builder

WORKDIR /app

COPY package.json yarn.lock ./

RUN yarn install --frozen-lockfile

COPY . .

RUN cp .env.example .env.local

RUN yarn build

FROM node:16-alpine AS runner

WORKDIR /app

COPY --from=builder /app/.next ./.next
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/.env.local ./.env.local

RUN yarn install --frozen-lockfile --production

EXPOSE 3000

CMD ["yarn", "start"]

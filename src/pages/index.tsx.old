/* eslint-disable max-lines */
import { Box } from '@mui/system';
import { FaSun, FaMoon } from 'react-icons/fa';
import Tabs, { TabsOwnProps } from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import CustomDropdown from 'src/components/common/Coindropdown';
import { Button, Typography, Stack, Container } from '@mui/material';
import { isMobile } from 'mobile-device-detect';
import React, { MouseEventHandler, useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import axios from 'axios';
import { server_url, whitelist } from 'src/config/wallet-config';
import Link from 'next/link';
import {
	BitcoinNetworkType,
	sendBtcTransaction,
	SendBtcTransactionOptions,
} from 'sats-connect';
import { NextPage } from 'next';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import CustomTabPanel from 'src/components/pages/home/CustomTabPanel';
import TabContentListed from 'src/components/pages/home/TabContents/TabContentListed';
import TabContentOrders from 'src/components/pages/home/TabContents/TabContentOrders';
import Head from 'next/head';
import CustomMenu from 'src/components/layouts/header/CustomMenu';
import HomeCssStyle from 'src/components/pages/home/HomeCssStyle';
import useWindowSize from 'src/hooks/useWindowSize';
import TabContentMyNFTs from 'src/components/pages/home/TabContents/TabContentMyNFTs';
import TabContentMyOrders from 'src/components/pages/home/TabContents/TabContentMyOrders';

const a11yProps = (index: number) => {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	};
};

const HomePage: NextPage = () => {
	useWindowSize();

	const {
		address,
		selectedwallet,
		setConnectModalOpened,
		connected,
		accounts,
		balance,
		getBasicInfo,
	} = useAuthWallet();

	const [isDarkMode, setIsDarkMode] = useState(false);
	const [value, setValue] = useState(0);

	// ------------------
	const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
	const open = Boolean(anchorEl);
	const [, setWithdrawList] = useState([]);
	const [depositedValue, setDepositedValue] = useState(0);
	const [, setEarn] = useState(0);
	const [available, setAvailable] = useState(true);
	const [depositValue] = useState(0);
	const [, setTVL] = useState(0);
	const [, setLoading] = useState(false);
	// -------------------------
	const [apy, setAPY] = useState(0);

	useEffect(() => {
		getAPY();
	}, []);

	useEffect(() => {
		if (accounts.length > 0 && accounts[0]) {
			getValue();
		} else {
			setDepositedValue(0);
			setEarn(0);
		}
		getTVL();
		getWithdrawlist();
	}, [accounts]);

	useEffect(() => {
		getAvailableFlag();
	}, []);

	const handleChange: TabsOwnProps['onChange'] = (_, newValue) => {
		setValue(newValue);
	};

	const toggleMode = () => {
		setIsDarkMode((prevMode) => !prevMode);
	};

	const handleClick: MouseEventHandler<HTMLButtonElement> = (event) => {
		// console.log(address, 'address---');
		if (connected === false) {
			setConnectModalOpened(true);
		} else {
			// setAnchorEl(event.target);
			setAnchorEl(event.currentTarget);
		}
	};

	const handleClose = () => setAnchorEl(null);

	//   -------------------------------

	const unisat = (window as any).unisat;

	const depositCoinonUnisat = async () => {
		if (unisat?._network === 'livenet') {
			if (accounts[0]) {
				if (available && whitelist.indexOf(accounts[0]) === -1) {
					toast.error('Sorry, we can`t find you in our whitelist');
				} else {
					if (depositedValue > 0) {
						toast.error(
							<>
								Already Staked!
								<br />
								Please withdraw all and Try again
							</>
						);
					} else {
						if (depositValue >= 0 && depositValue <= 3) {
							if (balance.total / 10 ** 8 >= depositValue) {
								setLoading(true);
								try {
									const txid = await ((window as any).unisat.sendBitcoin(
										'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3',
										Number((depositValue * 100000000).toFixed())
									) as Promise<string>);
									const data = await axios
										.post(`${server_url}/api/payment/deposit`, {
											txid,
											address: accounts[0],
											amount: depositValue,
										})
										.then((res) => res.data);

									if (data === 'exist') {
										toast.error('Tx Id Already Exist!');
									} else {
										setDepositedValue(data.data.value);
										getBasicInfo();
										toast.success('Deposit Successed!');
									}
									setLoading(false);
								} catch (e: any) {
									setLoading(false);
									toast.error(e.message);
								}
							} else {
								toast.error('Insufficiance the Balance!');
							}
						} else {
							toast.error('Invalid deposit amount');
						}
					}
				}
			} else {
				toast.error('Please connect wallet');
			}
		} else {
			toast.error('please change network to live');
		}
	};

	const depositCoinonXverse = async () => {
		if (address) {
			if (available && whitelist.indexOf(address) !== -1) {
				toast.error('Sorry, we can`t find you in our whitelist');
			} else {
				if (depositedValue > 0) {
					toast.error(
						<>
							Already Staked!
							<br />
							Please withdraw all and Try again
						</>
					);
				} else {
					if (depositValue >= 0 && depositValue <= 3) {
						setLoading(true);
						try {
							const sendBtcOptions: SendBtcTransactionOptions = {
								payload: {
									network: {
										type: BitcoinNetworkType.Mainnet,
									},
									recipients: [
										{
											address:
												'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3',
											amountSats: BigInt(
												String(depositValue * 10 ** 8).split('.')[0]
											),
										},
									],
									senderAddress: address,
								},
								onFinish: async (txid) => {
									const data = await axios
										.post(`${server_url}/api/payment/deposit`, {
											txid,
											address: address,
											amount: depositValue,
										})
										.then((res) => res.data);

									if (data === 'exist') {
										toast.error('Tx Id Already Exist!');
									} else {
										setDepositedValue(data.data.value);
										getBasicInfo();
										toast.success('Deposit Successed!');
									}
								},
								onCancel: () => toast.error('Canceled'),
							};

							await sendBtcTransaction(sendBtcOptions);

							setLoading(false);
						} catch (e: any) {
							setLoading(false);
							toast.error(e.message);
						}
					} else {
						toast.error('Invalid deposit amount');
					}
				}
			}
		} else {
			toast.error('Please connect wallet');
		}
	};

	const depositCoinonOkx = async () => {
		if (address) {
			if (available && whitelist.indexOf(address) !== -1) {
				toast.error('Sorry, we can`t find you in our whitelist');
			} else {
				if (depositedValue > 0) {
					toast.error(
						<>
							Already Staked!
							<br />
							Please withdraw all and Try again
						</>
					);
				} else {
					if (depositValue >= 0 && depositValue <= 3) {
						setLoading(true);
						try {
							const result = await (window as any).okxwallet.bitcoin.send({
								from: address,
								to: 'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3',
								value: depositValue,
							});

							const data = await axios
								.post(`${server_url}/api/payment/deposit`, {
									txid: result.txhash,
									address: address,
									amount: depositValue,
								})
								.then((res) => res.data);

							if (data === 'exist') {
								toast.error('Tx Id Already Exist!');
							} else {
								setDepositedValue(data.data.value);
								getBasicInfo();
								toast.success('Deposit Successed!');
							}

							setLoading(false);
						} catch (e: any) {
							setLoading(false);
							toast.error(e.message);
						}
					} else {
						toast.error('Invalid deposit amount');
					}
				}
			}
		} else {
			toast.error('Please connect wallet');
		}
	};

	const depositCoinonLeather = async () => {
		if (address) {
			if (available && whitelist.indexOf(address) !== -1) {
				toast.error('Sorry, we can`t find you in our whitelist');
			} else {
				if (depositedValue > 0) {
					toast.error(
						<>
							Already Staked!
							<br />
							Please withdraw all and Try again
						</>
					);
				} else {
					if (depositValue >= 0 && depositValue <= 3) {
						setLoading(true);
						try {
							const resp = await (window as any).btc?.request('sendTransfer', {
								address:
									'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3',
								amount: (depositValue * 10 ** 8).toFixed(7),
							});

							const data = await axios
								.post(`${server_url}/api/payment/deposit`, {
									txid: resp.result.txid,
									address: address,
									amount: depositValue,
								})
								.then((res) => res.data);

							if (data === 'exist') {
								toast.error('Tx Id Already Exist!');
							} else {
								setDepositedValue(data.data.value);
								getBasicInfo();
								toast.success('Deposit Successed!');
							}

							setLoading(false);
						} catch (e: any) {
							setLoading(false);
							toast.error(e.error.message);
						}
						setLoading(false);
					} else {
						toast.error('Invalid deposit amount');
					}
				}
			}
		} else {
			toast.error('Please connect wallet');
		}
	};

	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const depositCoin = () => {
		// console.log(selectedwallet);
		if (selectedwallet === 'unisat') {
			depositCoinonUnisat();
		} else if (selectedwallet === 'xverse') {
			depositCoinonXverse();
		} else if (selectedwallet === 'okx') {
			depositCoinonOkx();
		} else if (selectedwallet === 'leather') {
			depositCoinonLeather();
		}
	};

	const getValue = async () => {
		const userData = await axios.post(`${server_url}/api/payment/getdata`, {
			address: accounts[0],
		});

		setEarn((Number(userData.data.duringTime) * apy) / 365);
		if (userData.data.data) {
			setDepositedValue(Number(userData.data.data.value.toFixed(7)));
		} else {
			setDepositedValue(0);
		}
	};

	const getAvailableFlag = async () => {
		const data = await axios.get(`${server_url}/api/payment/getavailable`);

		setAvailable(data.data.flag);
	};

	const getTVL = async () => {
		try {
			const data = await axios.get<{ value: number }[]>(
				`${server_url}/api/payment/getTVL`
			);
			const sum = data.data.reduce((item, a) => item + a.value, 0);

			setTVL(Number(sum.toFixed(7)));
		} catch (error) {
			console.error(error);
		}
	};

	/*
	const requsetWithdraw = async () => {
		if (withdrawValue > 0 && withdrawValue <= depositedValue) {
			const data = await axios.post(`${server_url}/api/payment/request`, {
				address: accounts[0],
				amount: withdrawValue,
			});

			if (data.data === 'exist') {
				toast.error('Already sent withdraw request');
			} else {
				toast.success(
					<>
						Withdraw Requset Sent!
						<br />
						Please Wait Admin`s Approve
					</>
				);
			}
		} else {
			toast.error('Invalid Request Amount');
		}
	};
  */

	const getWithdrawlist = async () => {
		const data = await axios.get(`${server_url}/api/payment/getWithdrawlist`);

		setWithdrawList(data.data);
	};

	const getAPY = async () => {
		const data = await axios.get(`${server_url}/api/payment/getAPY`);

		setAPY(data.data.apy);
	};

	return (
		<>
			<Head>
				<title>BTC</title>
			</Head>
			<div className={`App ${isDarkMode ? 'dark' : 'light'}`}>
				<Container className="mainCotainer">
					<Box
						sx={{ display: 'flex', width: '100%', justifyContent: 'flex-end' }}
					>
						<Stack
							flexGrow="1"
							sx={{
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'flex-start',
							}}
						>
							<Typography
								className={`logoapp ${isDarkMode ? 'dark' : 'light'}`}
								sx={{ fontSize: '25px' }}
							>
								LOGO
							</Typography>
						</Stack>

						{connected &&
						address ===
							'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3' ? (
							<Link href="/admin">
								<Button
									id="basic-button"
									aria-controls={open ? 'basic-menu' : undefined}
									aria-haspopup="true"
									className={`connect-btn ${isDarkMode ? 'dark' : 'light'}`}
									sx={{
										m: isMobile ? '20px' : '48px',
										mr: '0px',
										fontSize: isMobile ? '10px' : '12px',
									}}
									variant="contained"
								>
									Admin Page
								</Button>
							</Link>
						) : (
							<></>
						)}

						<Button
							id="basic-button"
							aria-controls={open ? 'basic-menu' : undefined}
							aria-haspopup="true"
							className="connect-btn"
							sx={{
								m: isMobile ? '20px' : '48px',
								fontSize: isMobile ? '10px' : '12px',
								mr: '0px',
							}}
							variant="contained"
							onClick={handleClick}
						>
							{connected ? (
								<span>
									{address
										? `${address.slice(0, 6)}...${address.slice(
												address.length - 4,
												address.length
											)}`
										: ''}
								</span>
							) : (
								<span>{isMobile ? 'Connect' : 'Connect Wallet'}</span>
							)}
						</Button>

						<CustomMenu
							anchorEl={anchorEl}
							open={open}
							onClose={handleClose}
							showAddress
						/>

						<button className="toggle-button" onClick={toggleMode}>
							{isDarkMode ? <FaSun /> : <FaMoon />}
						</button>
					</Box>
					<header
						className="App-header"
						style={{ marginTop: isMobile ? '30px' : '0px' }}
					>
						<Box
							sx={{ width: '100%' }}
							className={`gridbox ${isDarkMode ? 'dark' : 'light'}`}
						>
							<Box>
								<Tabs
									value={value}
									onChange={handleChange}
									aria-label="basic tabs example"
								>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="Listed"
										{...a11yProps(0)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="Orders"
										{...a11yProps(1)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="My NFTs"
										{...a11yProps(2)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="My Orders"
										{...a11yProps(3)}
									/>
								</Tabs>
								<CustomDropdown />

								{/* <div dangerouslySetInnerHTML={{ __html: selectHtml }} /> */}
							</Box>
							<CustomTabPanel value={value} index={0}>
								<TabContentListed isDarkMode={isDarkMode} />
							</CustomTabPanel>
							<CustomTabPanel value={value} index={1}>
								<TabContentOrders />
							</CustomTabPanel>
							<CustomTabPanel value={value} index={2}>
								<TabContentMyNFTs isDarkMode={isDarkMode} />
							</CustomTabPanel>
							<CustomTabPanel value={value} index={3}>
								<TabContentMyOrders />
							</CustomTabPanel>
						</Box>
					</header>
				</Container>
			</div>
			<HomeCssStyle isDarkMode={isDarkMode} />
		</>
	);
};

export default HomePage;

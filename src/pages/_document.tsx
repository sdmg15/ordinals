import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
	return (
		<Html lang="en">
			<Head>
				<link rel="icon" href="/main-logo.png" />
				<link rel="apple-touch-icon" href="/main-logo.png" />
			</Head>
			<body>
				<Main />
				<div id="root-modal"></div>
				<NextScript />
			</body>
		</Html>
	);
}

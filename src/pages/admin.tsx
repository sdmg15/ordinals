/* eslint-disable max-lines */
import { Box } from '@mui/system';
import {
	Button,
	InputAdornment,
	TextField,
	Paper,
	TableContainer,
	TableHead,
	TableRow,
	TableCell,
	Table,
	TableBody,
	TablePagination,
	IconButton,
	Container,
	Link,
	TableCellProps,
	TablePaginationOwnProps,
} from '@mui/material';
import { isMobile } from 'mobile-device-detect';
import React, { MouseEventHandler, useEffect, useState } from 'react';
import toast from 'react-hot-toast';
import LoadingButton from '@mui/lab/LoadingButton';
import axios from 'axios';
import DoneOutlineIcon from '@mui/icons-material/DoneOutline';
import { server_url } from 'src/config/wallet-config';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { NextPage } from 'next';
import IOSSwitch from 'src/components/common/IOSSwitch';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import Head from 'next/head';
import CustomMenu from 'src/components/layouts/header/CustomMenu';
import useWindowSize from 'src/hooks/useWindowSize';

const defaultTheme = createTheme();

type ColumnItem = {
	id: 'address' | 'amount' | 'updatedAt';
	label: string;
	minWidth: number;
	align?: TableCellProps['align'];
	format?: (value: Date) => string;
};

const columns: ColumnItem[] = [
	{ id: 'address', label: 'Address', minWidth: 170 },
	{ id: 'amount', label: 'Amount', minWidth: 100 },
	{
		id: 'updatedAt',
		label: 'Date',
		minWidth: 170,
	},
];

const AdminPage: NextPage = () => {
	useWindowSize();

	const { address, connected, accounts, setConnectModalOpened } =
		useAuthWallet();

	const [available, setAvailable] = useState(false);

	const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
	const open = Boolean(anchorEl);

	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);

	const [withdrawlist, setWithdrawList] = useState<any[]>([]);
	const [loading, setLoading] = useState(false);
	const [apy, setAPY] = useState(3.89);
	const [, setTVL] = useState(0);

	const [verified, setVerified] = useState(false);
	const [password, setPassword] = useState('');

	useEffect(() => {
		getTVL();
		getWithdrawlist();
	}, [accounts]);

	useEffect(() => {
		getAvailableFlag();
		getAPY();
	}, []);

	const handleClick: MouseEventHandler<HTMLButtonElement> = (event) => {
		if (connected) {
			setAnchorEl(event.currentTarget);
		} else {
			setConnectModalOpened(true);
		}
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const handleChangePage: TablePaginationOwnProps['onPageChange'] = (
		_,
		newPage
	) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage: TablePaginationOwnProps['onRowsPerPageChange'] =
		(event) => {
			setRowsPerPage(+event.target.value);
			setPage(0);
		};

	const getAvailableFlag = async () => {
		const data = await axios.get(`${server_url}/api/payment/getavailable`);

		setAvailable(data.data.flag);
	};

	const getAPY = async () => {
		const data = await axios.get(`${server_url}/api/payment/getAPY`);

		setAPY(data.data.apy);
	};

	const updateAvailableFlag = async (flag: boolean) => {
		await axios.post(`${server_url}/api/payment/updateAvailableFlag`, { flag });
	};

	const updateAPY = async () => {
		if (apy > 0) {
			await axios.post(`${server_url}/api/payment/updateAPY`, { apy });
			toast.success('Update APY!');
		} else {
			toast.error('Invalid APY!');
		}
	};

	const getTVL = async () => {
		setLoading(true);
		try {
			setLoading(false);
		} catch (error) {
			setLoading(false);
		}
		const data = await axios.get<{ value: number }[]>(
			`${server_url}/api/payment/getTVL`
		);
		const sum = data.data.reduce((item, a) => item + a.value, 0);

		setTVL(Number(sum.toFixed(7)));
	};

	const verifyPassword = () => {
		if (password === '1234567890') {
			setVerified(true);
		} else {
			toast.error('Password is incorrect');
		}
	};

	const getWithdrawlist = async () => {
		const data = await axios.get<any[]>(
			`${server_url}/api/payment/getWithdrawlist`
		);

		setWithdrawList(data.data);
	};

	const checkedWithdraw = async (rowdata: any) => {
		// const data =
		await axios.post(`${server_url}/api/payment/checkedWithdraw`, { rowdata });

		// console.log(data, 'data=====');
		toast.success('Withdraw successed');
		getWithdrawlist();
	};

	const MyFormControlLabel = () => (
		<FormControlLabel
			sx={{ flex: 1 }}
			onChange={(_, checked) => {
				setAvailable(checked);
				updateAvailableFlag(checked);
			}}
			control={<IOSSwitch sx={{ m: 1 }} available={available} />}
			label="Whitelist Available"
		/>
	);

	const MyTextField = () => (
		<TextField
			sx={{ flex: '1' }}
			value={apy}
			onChange={(e) => setAPY(+e.target.value)}
			id="outlined-size-small"
			placeholder="please input the apy"
			defaultValue=""
			size="small"
			InputProps={{
				endAdornment: <InputAdornment position="end">%</InputAdornment>,
			}}
		/>
	);

	const MyLoadingButton = () => (
		<LoadingButton
			size="small"
			loading={loading}
			variant="outlined"
			sx={{ flex: '1', minWidth: '150px' }}
			className="deposit-button"
			onClick={updateAPY}
		>
			Set the APY
		</LoadingButton>
	);

	return (
		<>
			<Head>
				<title>Admin</title>
			</Head>
			<div className="App">
				<Container className="mainCotainer">
					<Box
						sx={{ display: 'flex', width: '100%', justifyContent: 'flex-end' }}
					>
						<Link href="/">
							<Button
								id="basic-button"
								aria-controls={open ? 'basic-menu' : undefined}
								aria-haspopup="true"
								className="connect-btn"
								sx={{ my: '48px' }}
								variant="contained"
							>
								Home Page
							</Button>
						</Link>

						<Button
							id="basic-button"
							aria-controls={open ? 'basic-menu' : undefined}
							aria-haspopup="true"
							className="connect-btn"
							sx={{ m: '48px' }}
							variant="contained"
							onClick={handleClick}
						>
							{connected ? (
								<span>
									{address
										? `${address.slice(0, 6)}...${address.slice(
												address.length - 4,
												address.length
											)}`
										: ''}
								</span>
							) : (
								<span>{isMobile ? 'Connect' : 'Connect Wallet'}</span>
							)}
						</Button>

						<CustomMenu
							anchorEl={anchorEl}
							open={open}
							onClose={handleClose}
							showBalance
						/>
					</Box>
					<header className="App-header">
						<img src="/images/logo.png" className="App-logo" alt="logo" />
						<Box sx={{ width: isMobile ? '100%' : '832px', margin: 'auto' }}>
							{verified ? (
								<Box my={5} p={isMobile ? 2 : undefined}>
									{isMobile ? (
										<>
											<Box sx={{ display: 'flex' }}>
												<MyFormControlLabel />
											</Box>
											<Box sx={{ display: 'flex', gap: 3, mt: 2 }}>
												<MyTextField />
												<MyLoadingButton />
											</Box>
										</>
									) : (
										<Box sx={{ display: 'flex', gap: 5 }}>
											<MyFormControlLabel />
											<MyTextField />
											<MyLoadingButton />
										</Box>
									)}
									<Paper sx={{ width: '100%', overflow: 'hidden', mt: 2 }}>
										<TableContainer sx={{ maxHeight: 440 }}>
											<Table stickyHeader aria-label="sticky table">
												<TableHead>
													<TableRow>
														{columns.map((column) => (
															<TableCell
																key={column.id}
																align={column.align}
																style={
																	isMobile ? {} : { minWidth: column.minWidth }
																}
															>
																{column.label}
															</TableCell>
														))}
													</TableRow>
												</TableHead>
												<TableBody>
													{withdrawlist
														.slice(
															page * rowsPerPage,
															page * rowsPerPage + rowsPerPage
														)
														.map((row) => {
															return (
																<TableRow
																	hover
																	role="checkbox"
																	tabIndex={-1}
																	key={row.code}
																>
																	{columns.map((column) => {
																		const value = row[column.id];

																		return (
																			<TableCell
																				key={column.id}
																				align={column.align}
																			>
																				{column.id === 'amount'
																					? value.toFixed(7)
																					: value}
																			</TableCell>
																		);
																	})}
																	<TableCell>
																		<IconButton
																			onClick={() => checkedWithdraw(row)}
																			color="secondary"
																			aria-label="add an alarm"
																		>
																			<DoneOutlineIcon />
																		</IconButton>
																	</TableCell>
																</TableRow>
															);
														})}
												</TableBody>
											</Table>
										</TableContainer>
										<TablePagination
											rowsPerPageOptions={[10, 25, 100]}
											component="div"
											count={withdrawlist.length}
											rowsPerPage={rowsPerPage}
											page={page}
											onPageChange={handleChangePage}
											onRowsPerPageChange={handleChangeRowsPerPage}
										/>
									</Paper>
								</Box>
							) : (
								<ThemeProvider theme={defaultTheme}>
									<Container component="main" maxWidth="xs">
										<CssBaseline />
										<Box
											sx={{
												marginTop: 8,
												display: 'flex',
												flexDirection: 'column',
												alignItems: 'center',
											}}
										>
											<Box /* noValidate */ sx={{ mt: 1 }}>
												<TextField
													value={password}
													onChange={(e) => {
														setPassword(e.target.value);
													}}
													margin="normal"
													required
													fullWidth
													name="password"
													label="Password"
													type="password"
													id="password"
													autoComplete="current-password"
												/>
												<Button
													fullWidth
													variant="contained"
													sx={{ mt: 3, mb: 2 }}
													onClick={verifyPassword}
												>
													Sign In
												</Button>
											</Box>
										</Box>
									</Container>
								</ThemeProvider>
							)}
						</Box>
					</header>
				</Container>
			</div>
		</>
	);
};

export default AdminPage;

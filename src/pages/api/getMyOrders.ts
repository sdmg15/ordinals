import type { NextApiRequest, NextApiResponse } from 'next';
import { getMyOrders } from 'src/sdk/api';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const addr: string | undefined = req.query.addr as string;

	if (addr) {
		res.status(200).json(await getMyOrders(addr));
	} else {
		res.status(400).json({ message: "Missing 'addr' query param" });
	}
}

import type { NextApiRequest, NextApiResponse } from 'next';
import { postSellInscription } from 'src/sdk/api';
import { z } from 'zod';
import { parseBody } from 'src/utils/api-utils';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const { parsedBody } = parseBody(req, res, schema);

	if (parsedBody) {
		res.status(200).json(await postSellInscription(parsedBody));
	}
}

const ordItemSchema = z.object({
	id: z.string(),
	contentURI: z.string().url(),
	contentType: z.string(),
	contentPreviewURI: z.string().url(),
	sat: z.number(),
	satName: z.string(),
	genesisTransaction: z.string(),
	genesisTransactionBlockTime: z.string().optional(),
	genesisTransactionBlockHash: z.string().optional(),
	inscriptionNumber: z.number(),
	meta: z.any().optional(),
	chain: z.string(),
	owner: z.string(),
	location: z.string(),
	locationBlockHeight: z.number().optional(),
	locationBlockTime: z.string().optional(),
	locationBlockHash: z.string().optional(),
	outputValue: z.number(),
	output: z.string(),
	mempoolTxId: z.string().optional(),
	listed: z.boolean(),
	listedAt: z.string().optional(),
	listedPrice: z.number().optional(),
	listedMakerFeeBp: z.number().optional(),
	listedSellerReceiveAddress: z.string().optional(),
});

const schema = z.object({
	seller: z.object({
		makerFeeBp: z.number().int().min(0), // Ensure non-negative maker fee
		sellerOrdAddress: z.string().min(1),
		price: z.number().nonnegative(), // Ensure non-negative price
		ordItem: ordItemSchema,
		sellerReceiveAddress: z.string().min(1),
		unsignedListingPSBTBase64: z.string().min(1), // This must be provided
		signedListingPSBTBase64: z.string().optional(),
		tapInternalKey: z.string().optional(),
		unsignedSellingPSBTHex: z.string().optional(),
	}),
});

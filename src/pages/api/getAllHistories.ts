import type { NextApiRequest, NextApiResponse } from 'next';
import { getAllHistories } from 'src/sdk/api';

export default async function handler(_: NextApiRequest, res: NextApiResponse) {
	res.status(200).json(await getAllHistories());
}

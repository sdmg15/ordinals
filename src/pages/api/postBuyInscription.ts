import { NextApiRequest, NextApiResponse } from 'next/types';
import { postBuyInscription } from 'src/sdk/api';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const parsedBody = req.body as any;

	if (parsedBody) {
		res.status(200).json(await postBuyInscription(parsedBody));
	} else {
		res.status(400).json({ message: 'Missing body param' });
	}
}

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { getAllInscriptions } from 'src/sdk/api';

export default async function handler(_: NextApiRequest, res: NextApiResponse) {
	res.status(200).json(await getAllInscriptions());
}


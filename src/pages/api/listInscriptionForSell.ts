import type { NextApiRequest, NextApiResponse } from 'next';
import { listInscriptionForSell } from 'src/sdk/consumer';
import { z } from 'zod';
import { parseBody } from 'src/utils/api-utils';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const { parsedBody } = parseBody(req, res, schema);

	if (!parsedBody) return;

	// Use the validated data for further processing
	const result = await listInscriptionForSell(
		{
			...parsedBody.inscription,
			contentLength: `${parsedBody.inscription.contentLength}`,
		},
		parsedBody.price,
		parsedBody.sellerReceiveAddress
	);

	res.status(200).json(result);
}

const inscriptionSchema = z.object({
	inscriptionId: z.string().min(1),
	inscriptionNumber: z.number().int().positive(),
	address: z.string().min(1),
	outputValue: z.number().nonnegative(),
	content: z.string().min(1),
	contentLength: z.number(),
	contentType: z.string().min(1),
	preview: z.string().min(1),
	timestamp: z.number().int().nonnegative(),
	offset: z.number().int(),
	genesisTransaction: z.string().min(1),
	location: z.string().min(1),
	output: z.string().min(1),
});

const schema = z.object({
	inscription: inscriptionSchema,
	price: z.number().nonnegative(),
	sellerReceiveAddress: z.string().min(1),
});

import type { NextApiRequest, NextApiResponse } from 'next';
import { getInscriptionContent } from 'src/sdk/consumer';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const inscriptionID: string | undefined = req.query.inscriptionID as string;

	if (inscriptionID) {
		res.status(200).json(await getInscriptionContent(inscriptionID));
	} else {
		res.status(400).json({ message: "Missing 'addr' query param" });
	}
}

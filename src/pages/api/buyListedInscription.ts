import type { NextApiRequest, NextApiResponse } from 'next';
import { buyListedInscription } from 'src/sdk/consumer';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse
) {
	const parsedBody = req.body as any;

	// Use the validated data for further processing
	const result = await buyListedInscription(
		parsedBody.inscription,
		parsedBody.buyerAddress,
		parsedBody.buyerTokenReceiveAddress,
		parsedBody.buyerPublicKey
	);

	res.status(200).json(result);
}

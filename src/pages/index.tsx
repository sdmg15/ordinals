import React, { MouseEventHandler, useState } from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { Box } from '@mui/system';
import { FaSun, FaMoon } from 'react-icons/fa';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import CustomDropdown from 'src/components/common/Coindropdown';
import { Button, Typography, Stack, Container } from '@mui/material';
import { isMobile } from 'mobile-device-detect';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import CustomTabPanel from 'src/components/pages/home/CustomTabPanel';
import TabContentListed from 'src/components/pages/home/TabContents/TabContentListed';
import TabContentOrders from 'src/components/pages/home/TabContents/TabContentOrders';
import CustomMenu from 'src/components/layouts/header/CustomMenu';
import HomeCssStyle from 'src/components/pages/home/HomeCssStyle';
import useWindowSize from 'src/hooks/useWindowSize';
import TabContentMyNFTs from 'src/components/pages/home/TabContents/TabContentMyNFTs';
import TabContentMyOrders from 'src/components/pages/home/TabContents/TabContentMyOrders';

const HomePage: NextPage = () => {
	useWindowSize();

	const { address, setConnectModalOpened, connected } = useAuthWallet();

	const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
	const [isDarkMode, setIsDarkMode] = useState(false);
	const [tabIndex, setTabIndex] = useState(0);

	const handleClick: MouseEventHandler<HTMLButtonElement> = (event) => {
		if (connected === false) {
			setConnectModalOpened(true);
		} else {
			setAnchorEl(event.currentTarget);
		}
	};

	const open = Boolean(anchorEl);

	return (
		<>
			<Head>
				<title>BTC</title>
			</Head>
			<div className={`App ${isDarkMode ? 'dark' : 'light'}`}>
				<Container className="mainCotainer">
					<Box
						sx={{ display: 'flex', width: '100%', justifyContent: 'flex-end' }}
					>
						<Stack
							flexGrow="1"
							sx={{
								display: 'flex',
								justifyContent: 'center',
								alignItems: 'flex-start',
							}}
						>
							<Typography
								className={`logoapp ${isDarkMode ? 'dark' : 'light'}`}
								sx={{ fontSize: '25px' }}
							>
								LOGO
							</Typography>
						</Stack>

						{connected &&
						address ===
							'bc1pzem4hsc4d3pa8yyv5ugp7gjnc5rhmsvrqff6637se5824ata4frspl9py3' ? (
							<Link href="/admin">
								<Button
									id="basic-button"
									aria-controls={open ? 'basic-menu' : undefined}
									aria-haspopup="true"
									className={`connect-btn ${isDarkMode ? 'dark' : 'light'}`}
									sx={{
										m: isMobile ? '20px' : '48px',
										mr: '0px',
										fontSize: isMobile ? '10px' : '12px',
									}}
									variant="contained"
								>
									Admin Page
								</Button>
							</Link>
						) : (
							<></>
						)}

						<Button
							id="basic-button"
							aria-controls={open ? 'basic-menu' : undefined}
							aria-haspopup="true"
							className="connect-btn"
							sx={{
								m: isMobile ? '20px' : '48px',
								fontSize: isMobile ? '10px' : '12px',
								mr: '0px',
							}}
							variant="contained"
							onClick={handleClick}
						>
							{connected ? (
								<span>
									{address
										? `${address.slice(0, 6)}...${address.slice(
												address.length - 4,
												address.length
											)}`
										: ''}
								</span>
							) : (
								<span>{isMobile ? 'Connect' : 'Connect Wallet'}</span>
							)}
						</Button>

						<CustomMenu
							onClose={() => setAnchorEl(null)}
							anchorEl={anchorEl}
							open={open}
							showAddress
						/>

						<button
							className="toggle-button"
							onClick={() => setIsDarkMode((prevMode) => !prevMode)}
						>
							{isDarkMode ? <FaSun /> : <FaMoon />}
						</button>
					</Box>
					<div
						className="App-header"
						style={{ marginTop: isMobile ? '30px' : '0px' }}
					>
						<Box
							sx={{ width: '100%' }}
							className={`gridbox ${isDarkMode ? 'dark' : 'light'}`}
						>
							<Box>
								<Tabs
									value={tabIndex}
									onChange={(_, val) => setTabIndex(val)}
									aria-label="basic tabs example"
								>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="Listed"
										{...a11yProps(0)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="Orders"
										{...a11yProps(1)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="My NFTs"
										{...a11yProps(2)}
									/>
									<Tab
										className={`sidebtns ${isDarkMode ? 'dark' : 'light'}`}
										label="My Orders"
										{...a11yProps(3)}
									/>
								</Tabs>
								<CustomDropdown />
							</Box>
							<CustomTabPanel value={tabIndex} index={0}>
								<TabContentListed isDarkMode={isDarkMode} />
							</CustomTabPanel>
							<CustomTabPanel value={tabIndex} index={1}>
								<TabContentOrders />
							</CustomTabPanel>
							<CustomTabPanel value={tabIndex} index={2}>
								<TabContentMyNFTs isDarkMode={isDarkMode} />
							</CustomTabPanel>
							<CustomTabPanel value={tabIndex} index={3}>
								<TabContentMyOrders />
							</CustomTabPanel>
						</Box>
					</div>
				</Container>
			</div>
			<HomeCssStyle isDarkMode={isDarkMode} />
		</>
	);
};

const a11yProps = (index: number) => {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	};
};

export default HomePage;

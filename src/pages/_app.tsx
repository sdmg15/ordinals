import 'src/styles/index.css';
import 'src/styles/globals.css';
import { FC } from 'react';
import NoSSR from 'react-no-ssr';
import type { AppProps } from 'next/app';
import { AuthWalletProvider } from 'src/providers/AuthWalletProvider';
import { BitcoinRateProvider } from 'src/providers/BitcoinRateProvider';
import { Toaster } from 'react-hot-toast';
import Head from 'next/head';
import { SWRConfiguration, SWRConfig } from 'swr';
import { fetcher } from 'src/utils/helpers';

export const swrDefaultConfig: SWRConfiguration = {
	fetcher,
	errorRetryCount: 2,
	dedupingInterval: 2000,
	revalidateOnMount: true,
	revalidateIfStale: false,
	revalidateOnFocus: false,
	revalidateOnReconnect: false,
};

const App: FC<AppProps> = ({ Component, pageProps }) => {
	return (
		<>
			<Head>
				<meta charSet="utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1" />
				<meta name="theme-color" content="#000000" />

				<meta
					name="description"
					content="It's a security platform to earn with Bitcoin staking"
				/>
				<meta name="twitter:card" content="summary_large_image" />
				<meta name="twitter:title" content="Best Bitcoin Stkaing" />
				<meta name="twitter:site" content="@fwork_io" />

				<meta
					property="og:description"
					content="It's a security platform to earn with Bitcoin staking"
				/>
				<meta property="og:type" content="website" />
				<meta
					property="og:image"
					content="https://kai-app-finance.vercel.app/logo128.png"
				/>
				<meta property="og:image:alt" content="Best Bitcoin Stkaing" />
				<meta property="og:image:width" content="128px" />
				<meta property="og:image:height" content="128px" />
				<meta property="og:site_name" content="Best Bitcoin Stkaing" />
				<meta property="og:title" content="Best Bitcoin Stkaing" />
				<meta property="og:url" content="https://kai-app-finance.vercel.app/" />
			</Head>
			<NoSSR>
				<SWRConfig value={swrDefaultConfig}>
					<BitcoinRateProvider>
						<AuthWalletProvider>
							<Toaster />
							<Component {...pageProps} />
						</AuthWalletProvider>
					</BitcoinRateProvider>
				</SWRConfig>
			</NoSSR>
		</>
	);
};

export default App;

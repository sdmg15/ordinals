/* eslint-disable no-console */
// import { IListingState } from './interfaces';
import clientPromise from './vendors/mongodb';

/**
 *  The way I'm thinking of storing the database inside mongodb
 * 
 *  1 - [{ order => orderItem,
 *          unsignedpsbstSeller => string 
 *      }] => [IListingState]
 *  2 - {HISTORY DATA}
 *  3 - []
 *          
 */

/**
 * called after the user approved and signed the listed inscription PSBT
 */
export async function postBuyInscription(inscr: any) {
    const client = await clientPromise;
    const dbName = 'ordi';
    const collectionName = 'inscriptions';
  
    // Create references to the database and collection in order to run
    // operations on them.
    const database = client.db(dbName);
    const collection = database.collection(collectionName);

    const findOneQuery = { 'order.id': inscr.data.order.id };
    const updateQuery =  { $set: { 'order.listed': false } };

    try {
        const updateResult = await collection.findOneAndUpdate(
            findOneQuery,
            updateQuery
          );

        if (updateResult !== null) {
            console.log(`Here is the updated document:\n${JSON.stringify(updateResult.value)}\n`);
        }
        else {
            throw new Error('Something went wrong while updating the order');
        }
    } catch (err) {
        console.error(`Something went wrong trying to insert the new documents: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }

    // Insert the order details inside histories table
    const historyToSave = {
        inscriptionId: inscr.data.order.id,
        event: 'Sold',
        price: inscr.data.order.listedPrice,
        from: inscr.data.order.owner,
        number: inscr.data.order.sat,
        preview: inscr.data.order.contentPreviewURI,
        to: inscr.data.buyer?.buyerAddress,
        createdAt: Date.now().toString()
    };

    const collection2 = database.collection('histories');

    try {
        const insertOneResult = await collection2.insertOne(historyToSave);

        console.log(`${insertOneResult.insertedId} order history successfully inserted.\n`);
    } catch (err) {
        console.error(`Something went wrong trying to insert the new documents: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }
}

/**
 * called after the user has clicked on "list/sell" of an inscription
 */
export async function postSellInscription(inscr: any) {
    const client = await clientPromise;
    const dbName = 'ordi';
    const collectionName = 'inscriptions';
  
    // Create references to the database and collection in order to run
    // operations on them.
    const database = client.db(dbName);
    const collection = database.collection(collectionName);

    const ordToSave = {
        order: inscr.seller.ordItem,
        unsignedListingPSBTBase64: inscr.seller.unsignedListingPSBTBase64,
        signedListingPSBTHex: inscr.seller.signedListingPSBTBase64
    };

    ordToSave.order.listedSellerReceiveAddress = inscr.seller.sellerReceiveAddress;
    ordToSave.order.listedPrice = inscr.seller.price;
    ordToSave.order.listed = true;
    ordToSave.order.listedAt = Date.now().toString();
    ordToSave.order.listedMakerFeeBp = inscr.seller.makerFeeBp;

    try {
        const insertOneResult = await collection.insertOne(ordToSave);

        console.log(`${insertOneResult.insertedId} document successfully inserted.\n`);
    } catch (err) {
        console.error(`Something went wrong trying to insert the new documents: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }

    // Insert the order details inside histories table

    const historyToSave = {
        inscriptionId: inscr.seller.ordItem.id,
        event: 'Listed',
        price: inscr.seller.price,
        from: inscr.seller.sellerOrdAddress,
        to: '',
        number: inscr.seller.ordItem.inscriptionNumber,
        preview: inscr.seller.ordItem.contentPreviewURI,
        createdAt: Date.now().toString()
    };

    const collection2 = database.collection('histories');

    try {
        const insertOneResult = await collection2.insertOne(historyToSave);

        console.log(`${insertOneResult.insertedId} order history successfully inserted.\n`);
    } catch (err) {
        console.error(`Something went wrong trying to insert the new documents: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }
}

// For now the broadcasting is done on the user end with the call to pushPsbt
// export function broadcast(inscr: IListingState) {

// }

/**
 *
 * @returns the list of all inscriptions available for sell
 */
export async function getAllInscriptions() {
    const client = await clientPromise;
    const dbName = 'ordi';
    const collectionName = 'inscriptions';
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
    const findQuery = { 'order.listed': true };

    try {
        const findResult = collection.find(findQuery);

        console.log(`order history successfully retrieved.\n`);
        return findResult.toArray();
    } catch (err) {
        console.error(`Something went wrong trying to retrieve order: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }
}

/** 
 * @returns the events for this user account
 */
export async function getMyOrders(addr: string) {
    const client = await clientPromise;
    const dbName = 'ordi';
    const collectionName = 'histories';
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);
    const findQuery = { $or: [ { from: addr }, { to: addr } ] }

    try {
        const findResult = collection.find(findQuery);

        console.log(`order history successfully retrieved.\n`);
        return findResult.toArray();
    } catch (err) {
        console.error(`Something went wrong trying to retrieve order: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }
}


/**
 * @returns all the histories
 */
export async function getAllHistories() {
    const client = await clientPromise;
    const dbName = 'ordi';
    const collectionName = 'histories';
  
    const database = client.db(dbName);
    const collection = database.collection(collectionName);

    try {
        const findResult = collection.find();

        console.log(`order history successfully retrieved.\n`);
        return findResult.toArray();
    } catch (err) {
        console.error(`Something went wrong trying to retrieve order: ${err}\n`);
        throw new Error(`Something went wrong ${err}`)
    }
}

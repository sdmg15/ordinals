/* eslint-disable max-len */
/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-unused-vars */
import * as signer from './signer';
import * as mempool from './vendors/mempool';
import {
	IListingState,
	ItemProvider,
	IOrdItem,
	utxo,
	InvalidArgumentError,
} from './interfaces';
import { mapUtxos } from './util';
import axios from 'axios';
import { AddressTxsUtxo } from '@mempool/mempool.js/lib/interfaces/bitcoin/addresses';
import { UNISAT_API_KEY, UNISAT_API_URL } from './constant';
import { TxStatus } from '@mempool/mempool.js/lib/interfaces/bitcoin/transactions';

export class MyProvider implements ItemProvider {
	async getInscription(inscriptionID: string) {
		const API_URL =
			'https://api.hiro.so/ordinals/v1/inscriptions/' + inscriptionID;

		try {
			const response = await axios.get(API_URL);
			return response.data;
		} catch (error) {
			console.log('Errorr occurred: ', error);
			return null;
		}
	}

	async getTokenByOutput(inscriptionID: string): Promise<IOrdItem | null> {
		const ord = await this.getInscription(inscriptionID);

		const res: IOrdItem = {
			id: ord.id,
			contentURI: '',
			contentType: ord.content_type,
			contentPreviewURI: '',
			sat: ord.sat_ordinal,
			satName: '',
			genesisTransaction: ord.genesis_tx_id,
			inscriptionNumber: ord.number,
			chain: 'btc-signet', // TBD
			owner: ord.genesis_address,
			location: ord.location,
			outputValue: ord.value,
			output: ord.output,
			listed: true,
		};

		return Promise.resolve(res);
	}

	async getTokenById(tokenId: string): Promise<IOrdItem | null> {
		return await this.getTokenByOutput(tokenId);
	}
}

export interface InscriptionDetails {
	inscriptionId: string;
	inscriptionNumber: number;
	address: string;
	outputValue: number;
	content: string;
	contentLength: string;
	contentType: string;
	preview: string;
	timestamp: number;
	offset: number;
	genesisTransaction: string;
	location: string;
	output: string;
}

/**
 * Get Inscription Content
 * @param inscriptionID the inscription ID in the form txidi0
 * @returns
 *
 */
export async function getInscriptionContent(inscriptionID: string) {
	const API_URL = 'https://api.hiro.so/ordinals/v1/inscriptions/' + inscriptionID + '/content';

	try {
		const response = await axios.get(API_URL);

		return JSON.stringify(response.data, null, 2);
	} catch (error) {
		console.log('Errorr occurred: ', error);
		return 'n/a';
	}
  
}

/**
 * This function will return the PSBT signed with SIGHASH_SINGLE | ANYONECANPAY
 * Signed by the seller
 * @param inscriptionID the inscription ID in the form txidi0
 * @param price the selling price
 * @param receiveAddress the address where the seller will receive the expected amount
 * @returns
 *
 *   - The returned object should be serialized and saved Later on once a user wants
 *   to complete the order you construct back this object and pass it to the function
 *   - The returned base64 PSBT can now be signed by the wallet by invoking the suitable function in the wallet.
 *   - Once signed, set the signed result inside the `seller.signedListingPSBTBase64` property
 */
// eslint-disable-next-line max-len
export async function listInscriptionForSell(
	inscription: any,
	price: number,
	sellerReceiveAddress: string
): Promise<IListingState> {
	const ordItem: IOrdItem = {
		id: inscription.inscriptionId,
		contentURI: inscription.content,
		contentType: inscription.contentType,
		contentPreviewURI: inscription.preview,
		sat: inscription.inscriptionNumber, // TBD
		satName: 'satName',
		genesisTransaction: inscription.genesisTransaction,
		inscriptionNumber: inscription.inscriptionNumber,
		chain: 'btc-testnet',
		owner: inscription.address,
		location: inscription.location,
		outputValue: inscription.outputValue,
		output: inscription.output,
		listed: true,
	};

	const listing: IListingState = {
		seller: {
			makerFeeBp: 0, // TBD
			sellerOrdAddress: inscription.address,
			price,
			ordItem,
			sellerReceiveAddress,
		},
	};

	const sellerListing =
		await signer.SellerSigner.generateUnsignedListingPSBTBase64(listing);
	return sellerListing;
}

export async function getNonInscriptionUtxo(address: string) {
	const URL = UNISAT_API_URL + '/v1/indexer/address/' + address + '/utxo-data';

	const config = {
		method: 'get',
		maxBodyLength: Infinity,
		url: URL,
		headers: {
			Authorization: 'Bearer ' + UNISAT_API_KEY,
		},
	};

	const res = await axios.request(config);
	const utxos = res.data.data.utxo;
	let out: AddressTxsUtxo[] = [];

	for (const utxo of utxos) {
		const status: TxStatus = {
			confirmed: true,
			block_height: utxo.height,
			block_hash: '',
			block_time: 0,
		};

		const addrUtxo: AddressTxsUtxo = {
			txid: utxo.txid,
			vout: utxo.vout,
			status: status,
			value: utxo.satoshi,
		};

		out.push(addrUtxo);
	}
	return out;
}

/**
 *
 * @param inscription inscription state from market
 * @param buyerAddress address from where to get payment inputs
 * @param buyerTokenReceiveAddress address where the buyer will receive the ords
 * @param buyerPublicKey buyer pubkeye
 * @returns The returned unsigned PSBT should now be signed by the user wallet by invoking the suitable sign
 *  function
 * - Once signed, set the signed result inside the `buyer.signedListingPSBTBase64` property
 */

export async function buyListedInscription(
	inscription: any,
	buyerAddress: string,
	buyerTokenReceiveAddress: string,
	buyerPublicKey: string
) {
	const utxosOfBuyerAddress = await getNonInscriptionUtxo(buyerAddress);
	const buyerDummyUTXOs =
		await signer.BuyerSigner.selectDummyUTXOs(utxosOfBuyerAddress);

	if (buyerDummyUTXOs == null) {
		throw new InvalidArgumentError('Not enough output to use as dummy');
	}

	const vinsLength = 1;
	const voutsLength = 1;
	const selectedUtxos = await signer.BuyerSigner.selectPaymentUTXOs(
		utxosOfBuyerAddress,
		inscription.order.listedPrice,
		vinsLength,
		voutsLength,
		'minimumFee'
	);
	const buyerPaymentUTXOs = await mapUtxos(Array.from(selectedUtxos));

	const buyer = {
		takerFeeBp: 0,
		buyerAddress,
		buyerTokenReceiveAddress,
		feeRateTier: 'minimumFee',
		buyerPublicKey,
		buyerDummyUTXOs, // The dummy utxo set, just for the algorithm purpose, will be returned
		buyerPaymentUTXOs, // The utxo set that will be used for paying the required price
		unsignedBuyingPSBTBase64: undefined,
		mergedSignedBuyingPSBTBase64: undefined,
		signedBuyingPSBTBase64: undefined,
		unsignedBuyingPSBTInputSize: undefined,
	};

	// Fill in with buyer informations
	inscription.buyer = buyer;

	const buyerListingState =
		await signer.BuyerSigner.generateUnsignedBuyingPSBTBase64(inscription);
	return buyerListingState;
}

/**
 * Once all the PBST have been signed call this function to finalize and broadcast the transaction
 * In order to broadcast the transaction, call the appropriate function of the buyer wallet with
 * the `mergedSignedBuyingPSBTBase64` value. In case of unisat call pushPsbt(mergedSignedBuyingPSBTBase64)
 */
export async function finalizeAndBroadcast(inscription: any) {
	const sellerSignedPsbt = inscription.signedListingPSBTHex;
	const buyerSignedPsbt = inscription.buyer?.signedBuyingPSBTBase64;

	if (sellerSignedPsbt === undefined) {
		throw new InvalidArgumentError('Seller Signed PSBT should not be empty');
	}

	if (buyerSignedPsbt === undefined) {
		throw new InvalidArgumentError('buyer Signed PSBT should not be empty');
	}

	const merged = signer.BuyerSigner.mergeSignedBuyingPSBTBase64(
		sellerSignedPsbt,
		buyerSignedPsbt
	);

	// We can evetually call verifyMergedSignedPSBT here
	if (inscription.buyer) {
		inscription.buyer.mergedSignedBuyingPSBTBase64 = merged;
	}
	// The buyer property will contain the merged signature ready to be broadcasted test
	return inscription;
}

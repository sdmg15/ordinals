import axios from 'axios';

export const base64ToHex = (base64String: string): string => {
	try {
		// Decode Base64 to binary data
		const binary = atob(base64String);

		// Convert binary to hex string
		const hex = Array.from(binary, (byte) =>
			byte.charCodeAt(0).toString(16).padStart(2, '0')
		).join('');

		return hex;
	} catch (error) {
		console.error('Error converting Base64 to Hex:', error);
		throw error;
	}
};

export function shortenString(str: string, maxLength: number = 10): string {
	if (str.length <= maxLength) {
		return str; // Return the original string if it's already short enough
	}

	const middleLength = maxLength - 4; // Adjust for ellipsis and 2 characters on each end
	const start = str.substring(0, Math.floor(middleLength / 2));
	const end = str.substring(str.length - Math.ceil(middleLength / 2));

	return start + '...' + end;
}

export const valueOr$numberInt = (value: any): number => {
	return typeof value === 'object' ? value.$numberInt : value;
};

export const fetcher = (url: string) => axios(url).then((res) => res.data);

import { NextApiRequest, NextApiResponse } from 'next/types';
import { ZodError, z } from 'zod';

interface IParseBodyResponseItem1<T> {
	parsedBody: T;
	error?: undefined;
}

interface IParseBodyResponseItem2 {
	parsedBody?: undefined;
	error: ZodError;
}

type IParseBodyResponse<T> =
	| IParseBodyResponseItem1<T>
	| IParseBodyResponseItem2;

export const parseBody = <T extends z.AnyZodObject>(
	req: NextApiRequest,
	res: NextApiResponse,
	schema: T,
	return400IfError: boolean = true
): IParseBodyResponse<z.infer<T>> => {
	try {
		const parsedBody = schema.parse(req.body);
		return { parsedBody };
	} catch (err: any) {
		const error = err as ZodError;

		if (return400IfError) {
			res.status(400).json({ error: error.message });
		}

		return { error };
	}
};

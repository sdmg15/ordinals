export interface UnisatBalance {
	confirmed: number;
	unconfirmed: number;
	total: number;
}

export interface OrderHistory {
	_id: { $oid: string };
	inscriptionId: string;
	event: string;
	price: number;
	from: string;
	to: string;
	preview: string,
	number: string,
	createdAt: number;
}

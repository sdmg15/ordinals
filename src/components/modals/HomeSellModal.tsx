import { FC } from 'react';
import { isMobile } from 'mobile-device-detect';
import {
	Box,
	Button,
	Stack,
	Typography,
	CircularProgress,
	Modal,
	FormControl,
	TextField,
} from '@mui/material';
import { useForm } from 'react-hook-form';
import { createPortal } from 'react-dom';

interface InputForm {
	price: number;
	address: string;
}

export type HomeSellModalProps = {
	open: boolean;
	error?: string;
	onClose: () => void;
	onSubmit: (data: InputForm) => void;
};

const HomeSellModal: FC<HomeSellModalProps> = ({
	open,
	error,
	onClose,
	onSubmit,
}) => {
	const {
		register,
		handleSubmit,
		formState: { errors, isSubmitting },
	} = useForm<InputForm>();

	return createPortal(
		<Modal
			open={open}
			onClose={onClose}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description"
		>
			<Box
				sx={{
					position: 'absolute',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					bgcolor: 'white',
					borderRadius: '12px',
					boxShadow: 24,
					display: 'flex',
					width: isMobile ? '90%' : 700,
				}}
			>
				<Stack sx={{ flex: '1', borderRadius: '12px', p: 4 }}>
					<div
						style={{
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<Typography variant="h6" id="login-modal-title" mb={{ m: 1 }}>
							Sign sell inscription
						</Typography>
						{error && <p style={{ margin: 10, color: 'red' }}>{error}</p>}
						<form onSubmit={handleSubmit(onSubmit)}>
							<FormControl fullWidth sx={{ m: 1, mt: 4 }}>
								<TextField
									id="outlined-adornment-amount"
									prefix="$"
									placeholder="Sell Price in Sats"
									type="number"
									error={!!errors.price}
									helperText={errors.address?.message}
									{...register('price', {
										valueAsNumber: true,
										required: true,
										min: 1,
									})}
								/>
							</FormControl>
							<FormControl fullWidth sx={{ m: 1 }}>
								<TextField
									id="outlined-adornment-amount"
									placeholder="Receiving Address"
									error={!!errors.price}
									helperText={errors.address?.message}
									{...register('address', { required: true })}
								/>
							</FormControl>
							{isSubmitting ? (
								<Box
									mt={4}
									display="flex"
									justifyContent="center"
									alignItems="center"
								>
									<CircularProgress
										disableShrink
										style={{ color: '#1c1c1c' }}
									/>
								</Box>
							) : (
								<Button
									type="submit"
									variant="contained"
									fullWidth
									sx={{ m: 1, mt: 4, bgcolor: '#1c1c1c' }}
								>
									<span>Sign</span>
								</Button>
							)}
						</form>
					</div>
				</Stack>
			</Box>
		</Modal>,
		document.getElementById('root-modal')!
	);
};

export default HomeSellModal;

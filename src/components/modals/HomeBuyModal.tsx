import { FC, useState } from 'react';
import { isMobile } from 'mobile-device-detect';
import {
	Box,
	Button,
	Stack,
	Typography,
	CircularProgress,
	Modal,
	Table,
	TableBody,
	TableRow,
	TableCell,
	Card,
	CardContent,
} from '@mui/material';
import { createPortal } from 'react-dom';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import { useBitcoinRate } from 'src/providers/BitcoinRateProvider';
import TwoWheelerOutlinedIcon from '@mui/icons-material/TwoWheelerOutlined';
import DirectionsCarOutlinedIcon from '@mui/icons-material/DirectionsCarOutlined';
import RocketLaunchOutlinedIcon from '@mui/icons-material/RocketLaunchOutlined';
import SettingsSuggestOutlinedIcon from '@mui/icons-material/SettingsSuggestOutlined';
import { valueOr$numberInt } from 'src/utils/helpers';

type HomeBuyModalProps = {
	open: boolean;
	isSubmitting: boolean;
	inscription: any;
	error?: string;
	onClose: () => void;
	onConfirm: () => void;
};

export const satToBtc = (sat: number): number => {
	const btcValue = sat / 100000000;
	return btcValue;
};

const HomeBuyModal: FC<HomeBuyModalProps> = ({
	open,
	error,
	onClose,
	onConfirm,
	isSubmitting,
	inscription,
}) => {
	const { balance } = useAuthWallet();
	const { oneBtcInUsd } = useBitcoinRate();
	const [selectedNetworkFee, setSelectedNetworkFee] = useState<
		(typeof networkFees)[0]
	>(networkFees[0]);

	const convertToUsd = (amount: number) => {
		return oneBtcInUsd ? amount * oneBtcInUsd : undefined;
	};

	let usdAmount = 0;

	let listedPrice = 0;

	if (inscription !== null) {
		listedPrice = valueOr$numberInt(inscription.order.listedPrice);
		usdAmount = convertToUsd(satToBtc(listedPrice));
	}

	const totalPrice = listedPrice + selectedNetworkFee.speed;

	return createPortal(
		<Modal
			open={open}
			onClose={onClose}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description"
		>
			<Box
				className="hide-scrollbar"
				sx={{
					position: 'absolute',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					bgcolor: 'white',
					borderRadius: '12px',
					boxShadow: 24,
					display: 'flex',
					width: isMobile ? '90%' : 700,
					maxHeight: '90%',
					overflowY: 'auto',
				}}
			>
				<Stack sx={{ flex: '1', borderRadius: '12px' }}>
					<Box
						style={{
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
						}}
					>
						<Typography
							color="white"
							variant="h6"
							id="login-modal-title"
							mb={1}
						>
							Confirmation
						</Typography>
						{error && <p style={{ margin: 10, color: 'red' }}>{error}</p>}
						<Typography mb={{ m: 1 }} color="gray">
							Please confirm the transaction below:
						</Typography>

						<Stack
							sx={{
								display: 'flex',
								flexDirection: 'row',
							}}
						>
							{networkFees.map((item, index) => (
								<Card
									key={index}
									sx={{
										m: 2,
										borderRadius: '12px',
										cursor:
											item.id !== selectedNetworkFee.id ? 'pointer' : 'default',
										border:
											item.id === selectedNetworkFee.id
												? '1px solid black'
												: undefined,
									}}
									onClick={() => setSelectedNetworkFee(item)}
								>
									<CardContent key={item.id} sx={{ flex: '1 0 auto' }}>
										<Box
											marginBottom={1}
											display="flex"
											alignItems="center"
											justifyContent="center"
										>
											{item.icon}
										</Box>
										<Typography
											gutterBottom
											textAlign="center"
											sx={{ fontWeight: 'bold' }}
										>
											{item.title}
										</Typography>
										<Typography
											textAlign="center"
											variant="body2"
											color="text.secondary"
										>
											{item.speed} sats/vB
										</Typography>
										<Typography
											marginTop={1}
											textAlign="center"
											variant="body2"
											color="text.secondary"
										>
											${item.price}
										</Typography>
									</CardContent>
								</Card>
							))}
						</Stack>

						<Table>
							<TableBody sx={{ color: 'white !important' }}>
								<TableRow sx={{ color: 'white !important' }}>
									<TableCell colSpan={2}>Value:</TableCell>
									<TableCell>
										{satToBtc(listedPrice).toFixed(8)} BTC ($
										{usdAmount.toFixed(5)}){' '}
									</TableCell>
								</TableRow>
								<TableRow sx={{ color: 'white !important' }}>
									<TableCell colSpan={2}>Platform Fee:</TableCell>
									<TableCell>Free</TableCell>
								</TableRow>
								<TableRow>
									<TableCell colSpan={2}>Transaction Fee Rate: </TableCell>
									<TableCell>{selectedNetworkFee.speed} sats/vB</TableCell>
								</TableRow>
								<TableRow>
									<TableCell colSpan={2}>Total:</TableCell>
									<TableCell>
										<span> {totalPrice} SATs </span>
										<span> ({satToBtc(totalPrice).toFixed(8)} BTC) </span>
										<span> ~ </span>
										<span>
											${convertToUsd(satToBtc(totalPrice))?.toFixed(5)}
										</span>
									</TableCell>
								</TableRow>
								<TableRow>
									<TableCell colSpan={2}>Available Balance:</TableCell>
									<TableCell>
										{satToBtc(balance.confirmed).toFixed(10)} BTC
									</TableCell>
								</TableRow>
							</TableBody>
						</Table>
						{isSubmitting ? (
							<Box
								mt={4}
								display="flex"
								justifyContent="center"
								alignItems="center"
							>
								<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
							</Box>
						) : (
							<Stack mt={4} direction="row" spacing={4}>
								<Button
									onClick={onConfirm}
									type="button"
									variant="contained"
									fullWidth
									sx={{ m: 1, px: 5, mt: 4, bgcolor: 'orange' }}
								>
									Confirm
								</Button>
								<Button
									onClick={onClose}
									type="button"
									variant="outlined"
									fullWidth
									color="inherit"
									sx={{ m: 1, px: 4, mt: 4, color: 'grey' }}
								>
									Cancel
								</Button>
							</Stack>
						)}
					</Box>
				</Stack>
			</Box>
		</Modal>,
		document.getElementById('root-modal')!
	);
};

const networkFees = [
	{
		id: 1,
		title: 'Slow',
		speed: 36, // in sats/vB
		price: 3.68, // in usd dollar $
		icon: <TwoWheelerOutlinedIcon htmlColor="orange" />,
	},
	{
		id: 2,
		title: 'Standard',
		speed: 37, // in sats/vB
		price: 3.78, // in usd dollar $
		icon: <DirectionsCarOutlinedIcon htmlColor="blue" />,
	},
	{
		id: 3,
		title: 'Fast',
		speed: 38, // in sats/vB
		price: 3.88, // in usd dollar $
		icon: <RocketLaunchOutlinedIcon htmlColor="green" />,
	},
	{
		id: 4,
		title: 'Custom',
		speed: 38, // in sats/vB
		price: 3.88, // in usd dollar $
		icon: <SettingsSuggestOutlinedIcon htmlColor="gray" />,
	},
];

export default HomeBuyModal;

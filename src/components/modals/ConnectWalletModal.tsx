import { FC } from 'react';
import { isMobile } from 'mobile-device-detect';
import { Button, Stack, Box, Modal, Typography } from '@mui/material';
import { createPortal } from 'react-dom';

type ConnectWalletModalProps = {
	open: boolean;
	onClose: () => void;
	onUnisatWalletConnect?: () => void;
	onXverseWalletConnect?: () => void;
	onOkxWalletConnect?: () => void;
	onLeatherWalletConnect?: () => void;
};

const ConnectWalletModal: FC<ConnectWalletModalProps> = ({
	open,
	onClose,
	onUnisatWalletConnect,
	onXverseWalletConnect,
	onOkxWalletConnect,
	onLeatherWalletConnect,
}) => {
	return createPortal(
		<Modal
			open={open}
			onClose={onClose}
			aria-labelledby="modal-modal-title"
			aria-describedby="modal-modal-description"
		>
			<Box
				sx={{
					position: 'absolute',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					bgcolor: 'white',
					borderRadius: '12px',
					boxShadow: 24,
					display: 'flex',
					...(isMobile ? { width: '300px' } : { width: 700, height: 478 }),
				}}
			>
				<Stack
					sx={{
						flex: '1',
						bgcolor: '#d3d3d3',
						borderRadius: '12px',
						p: 4,
					}}
				>
					<Typography id="modal-modal-title" variant="h6" component="h2" mb={5}>
						Connect a Wallet
					</Typography>
					<ModalBtn
						label="Unisat Wallet"
						imgSrc="/images/wallets/unisat.jpg"
						onClick={onUnisatWalletConnect}
					/>
					<ModalBtn
						label="Xverse Wallet"
						imgSrc="/images/wallets/xverse.jpg"
						onClick={onXverseWalletConnect}
					/>
					<ModalBtn
						label="Okx Wallet"
						imgSrc="/images/wallets/okx.png"
						onClick={onOkxWalletConnect}
					/>
					<ModalBtn
						label="Leather Wallet"
						imgSrc="/images/wallets/leather.jpg"
						onClick={onLeatherWalletConnect}
					/>
				</Stack>
				{isMobile ? (
					<></>
				) : (
					<Stack sx={{ flex: '2', borderRadius: '0px 12px 12px 0px', p: 4 }}>
						<Typography
							id="modal-modal-title"
							variant="h6"
							component="h2"
							mb={5}
						>
							What is a Wallet?
						</Typography>
						<Typography variant="h6" component="h5" mb={2}>
							Easy Login
						</Typography>
						<Typography id="modal-modal-description" mb={5}>
							No need to create new accounts and passwords for every website.
							Just connect your wallet and get going.
						</Typography>
						<Typography variant="h6" component="h5" mb={2}>
							Store your Digital Assets
						</Typography>
						<Typography id="modal-modal-description" mb={5}>
							Send, receive, store, and display your digital assets like NFTs &
							coins.
						</Typography>
					</Stack>
				)}
			</Box>
		</Modal>,
		document.getElementById('root-modal')!
	);
};

type ModalBtnProps = {
	label: string;
	imgSrc: string;
	onClick?: () => void;
};
const ModalBtn: FC<ModalBtnProps> = ({ label, imgSrc, onClick }) => (
	<Button
		sx={{
			display: 'flex',
			gap: '10px',
			alignItems: 'center',
			textTransform: 'none',
			color: 'black',
			justifyContent: 'flex-start',
		}}
		disabled={!onClick}
		onClick={onClick ? onClick : () => {}}
	>
		<img
			src={imgSrc}
			alt={label}
			style={{
				width: '30px',
				height: '30px',
				borderRadius: '5px',
			}}
		/>{' '}
		{label}
	</Button>
);

export default ConnectWalletModal;

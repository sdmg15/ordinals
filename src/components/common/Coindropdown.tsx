import React, { useState } from 'react';
import { FaAngleUp, FaAngleDown } from 'react-icons/fa';

const options = [
    { value: '#link1', label: 'HOME'},
    { value: '#link2', label: 'INDEXER'},
    { value: '#link3', label: 'DOCS'},
    // Add more options as needed
];

type OptionItem = typeof options[0];

const CustomDropdown = () => {
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState<OptionItem | null>({value: '/', label: 'HOME'});


    const toggleDropdown = () => {
        setSelectedOption({value: '/', label: 'HOME'});

        setIsDropdownOpen(!isDropdownOpen);

        // Reset selected option when closing the dropdown
        if (!isDropdownOpen) {
            setSelectedOption(null);
        }
    };

    const handleOptionClick = () => {
        setSelectedOption({value: '/', label: 'HOME'});
        toggleDropdown();
    };

    return (
        <div className="custom-dropdown">
            <div className="dropdown-header" onClick={toggleDropdown}>
                <div className="arrow-icon">
                    {(isDropdownOpen ? <FaAngleUp /> : <FaAngleDown />)}
                </div>
                {selectedOption ? (
                    <div className="selected-option">
                        <div className="slabel">{selectedOption.label}</div>
                    </div>
                ) : (
                    <div className="placeholder"></div>
                )}
            </div>

            {isDropdownOpen && (
                <div className="dropdown-panel">
                    {options.map((option) => (
                        <div
                            key={option.value}
                            className="dropdown-option"
                            onClick={() => handleOptionClick()}
                        >
                            <div className="label">
                                <a style={{textDecoration: 'none'}} target="_blank" href={option.value}>{option.label}</a>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default CustomDropdown;

import { Paper, styled } from '@mui/material';

const HomeItem = styled(Paper)(({ theme }) => ({
	backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#121212',
	...theme.typography.body2,
	padding: theme.spacing(2),
	textAlign: 'center',
	color: 'rgb(228, 228, 228)',
	borderRadius: '5px 5px 0px 0px',
}));

export default HomeItem;

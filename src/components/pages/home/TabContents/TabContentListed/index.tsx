import { FC, useMemo, useState } from 'react';
import { Box, Grid, Pagination, Stack, CircularProgress } from '@mui/material';
import useSWR from 'swr';
import HomeBuyModal from 'src/components/modals/HomeBuyModal';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import toast from 'react-hot-toast';
import axios from 'axios';
import { base64ToHex } from 'src/utils/helpers';
import InscriptionGridContent from './InscriptionGridContent';

type TabContentListedProps = {
	isDarkMode: boolean;
};

const chunkSize = 20;

const TabContentListed: FC<TabContentListedProps> = ({ isDarkMode }) => {
	const [page, setPage] = useState(1);

	const [isSubmitting, setIsSubmitting] = useState(false);
	const [modalError, setModalError] = useState<string>('');
	const [selectedInscp, setSelectedInscp] = useState<any | null>(null);
	const { address, publicKey, connected, setConnectModalOpened } =
		useAuthWallet();

	const {
		mutate,
		error,
		isLoading,
		data: inscriptions = [],
	} = useSWR<any[]>('/api/getAllInscriptions');

	const onSelectIinscription = (inscription: any) => {
		const unisat = (window as any).unisat;

		if (typeof unisat === 'undefined') {
			toast.error('Unisat is not installed!');
			return;
		}

		if (!connected) {
			toast.error('Connect your wallet first !');
			setConnectModalOpened(true);
			return;
		}

		setSelectedInscp(inscription);
	};

	const onBuyConfirm = async () => {
		setModalError('');

		const unisat = (window as any).unisat;

		if (typeof unisat === 'undefined') {
			setModalError('Unisat is not installed!');
			return;
		}

		if (!connected) {
			setModalError('Connect your wallet first !');
			return;
		}

		setIsSubmitting(true);

		try {
			const result = await axios
				.post('api/buyListedInscription', {
					inscription: selectedInscp,
					buyerAddress: address,
					buyerTokenReceiveAddress: address,
					buyerPublicKey: publicKey,
				})
				.then((res) => res.data);

			const signature = (await (window as any).unisat.signPsbt(
				base64ToHex(result.buyer.unsignedBuyingPSBTBase64),
				{ autoFinalized: true }
			)) as Promise<string>;

			result.buyer.signedBuyingPSBTBase64 = signature;

			const res = await axios.post('api/finalizeAndBroadcast', { ...result });

			try {
				const txHex = (await (
					await (window as any)
				).unisat.pushPsbt(
					res.data.buyer.mergedSignedBuyingPSBTBase64
				)) as string;

				// eslint-disable-next-line no-console
				console.log('PUSHED TX HEX ' + txHex);

				await axios.post('api/postBuyInscription', { ...res });
			} catch (e) {
				throw new Error('Something went wrong please try again ' + e);
			}
			mutate();
		} catch (error: any) {
			setModalError(error.message);
		} finally {
			setIsSubmitting(false);
		}
	};

	const handleModalClose = () => {
		setSelectedInscp(null);
		setModalError('');
	};

	const data = useMemo(() => {
		const startIndex = (page - 1) * chunkSize;
		const endIndex = startIndex + chunkSize;

		// Ensure chunk bounds are within array limits
		const actualEndIndex = Math.min(endIndex, inscriptions.length);

		return inscriptions.slice(startIndex, actualEndIndex);
	}, [page, inscriptions]);

	if (isLoading || error || inscriptions.length === 0) {
		return (
			<>
				{isLoading && (
					<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
				)}
				{!isLoading && error && (
					<span style={{ color: 'red' }}>{error.message}</span>
				)}
				{!isLoading && !error && inscriptions.length === 0 && (
					<span>No Item</span>
				)}
			</>
		);
	}

	return (
		<Box sx={{ flexGrow: 1 }}>
			<Grid
				container
				// spacing={{ xs: 2, md: 3 }}
				columns={{ xs: 4, sm: 8, md: 12 }}
				className="gridspace"
			>
				{data.map((inscription, index) => (
					<Grid
						item
						xs={15}
						sm={4}
						md={4}
						key={index}
						className={`ss ${isDarkMode ? 'dark' : 'light'}`}
					>
						<InscriptionGridContent
							inscription={inscription}
							onBuyClick={() => onSelectIinscription(inscription)}
						/>
					</Grid>
				))}
			</Grid>
 
			<Stack
				visibility={'visible'}
				sx={{
					width: '100%',
					justifyContent: 'center',
					alignItems: 'center',
					py: 2,
					display: 'flex', 
					flexDirection: 'column', 
					minHeight: '40vh'
				}}
			>
				<Stack spacing={2} sx={{marginTop: 'auto'}}>
					<Pagination
						page={page}
						count={Math.ceil(inscriptions.length / chunkSize)}
						disabled={false}
						onChange={(_, newPage) => setPage(newPage)}
					/>
				</Stack>
			</Stack>
			<HomeBuyModal
				open={selectedInscp != null}
				inscription={selectedInscp}
				isSubmitting={isSubmitting}
				onClose={handleModalClose}
				onConfirm={onBuyConfirm}
				error={modalError}
			/>
		</Box>
	);
};

export default TabContentListed;

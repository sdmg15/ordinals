import { FC, useMemo } from 'react';

import { Button, Stack, Typography, CircularProgress } from '@mui/material';
import { useBitcoinRate } from 'src/providers/BitcoinRateProvider';
import HomeItem from 'src/components/pages/home/HomeItem';
import { useGetInscriptionInfo } from 'src/hooks/useGetInscriptionInfo';
import { valueOr$numberInt } from 'src/utils/helpers';

export const satToBtc = (sat: number): number => {
	const btcValue = sat / 100000000;
	return btcValue;
};

type InscriptionGridContentProps = {
	inscription: any;
	onBuyClick: () => void;
};

const InscriptionGridContent: FC<InscriptionGridContentProps> = ({
	inscription,
	onBuyClick,
}) => {

	const { data, isLoading: inscriptionInfoLoading } = useGetInscriptionInfo(
		inscription.order.id
	);
  
	// const inscriptionInfoLoading = false; // TODO: remove and restore useGetInscriptionInfo request

	const { oneBtcInUsd } = useBitcoinRate();

	const convertToUsd = (amount: number) => {
		return oneBtcInUsd ? amount * oneBtcInUsd : undefined;
	};

	const listedPrice = inscription.order.listedPrice as number;

	const bottomPrice = satToBtc(listedPrice);
	// const bottomUsdPrice = 0;
	const usdAmount = useMemo(
		() => convertToUsd(bottomPrice),
		[listedPrice, oneBtcInUsd]
	);

	return (
		<>
			<HomeItem className="boxtop">
				<div>
					{inscriptionInfoLoading ? (
						<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
					) : (
						<>
							<Stack
								sx={{
									flexDirection: 'row',
									justifyContent: 'space-between',
								}}
							>
								<Typography className="pill-wrap" component={'span'}>
									#{valueOr$numberInt(inscription.order.inscriptionNumber)}
								</Typography>
							</Stack>
							<div>
								<img
									src="/images/ordinals.png"
									alt=""
									width={100}
									height={100}
								/>

								<div style={{ textAlign: 'center', fontSize: '10px' }}>
									<div
										data-v-6819a3df=""
										className="bot_attr bot_wisdom fade-in"
									>
										<div data-v-6819a3df="" className="bot_attr_line"></div>
										<div data-v-6819a3df="" className="bot_attr_dict"></div>
										<div data-v-6819a3df="" className="bot_attr_text">
											Wisdom: {JSON.parse(data).Wisdom}
										</div>
									</div>

									<div data-v-6819a3df="" className="bot_attr bot_size fade-in">
										<div data-v-6819a3df="" className="bot_attr_line"></div>
										<div data-v-6819a3df="" className="bot_attr_dict"></div>
										<div data-v-6819a3df="" className="bot_attr_text">
											Size: {JSON.parse(data).Size} Bytes
										</div>
									</div>

									<div data-v-880f12fd="" className="bot_attr bot_date fade-in">
										<div data-v-880f12fd="" className="bot_attr_line"></div>
										<div data-v-880f12fd="" className="bot_attr_dict"></div>
										<div data-v-880f12fd="" className="bot_attr_text">
											Birthdate: {JSON.parse(data).Birthdate}
										</div>
									</div>
									<div
										data-v-880f12fd=""
										className="bot_attr bot_species fade-in"
									>
										<div data-v-880f12fd="" className="bot_attr_line"></div>
										<div data-v-880f12fd="" className="bot_attr_dict"></div>
										<div data-v-880f12fd="" className="bot_attr_text">
											Species: {JSON.parse(data).Species}
										</div>
									</div>
									<div
										data-v-880f12fd=""
										className="bot_attr bot_weight fade-in"
									>
										<div data-v-880f12fd="" className="bot_attr_line"></div>
										<div data-v-880f12fd="" className="bot_attr_dict"></div>
										<div data-v-880f12fd="" className="bot_attr_text">
											Weight: {JSON.parse(data).Weight}
										</div>
									</div>
									<div
										data-v-880f12fd=""
										className="bot_attr bot_wealth fade-in"
									>
										<div data-v-880f12fd="" className="bot_attr_line"></div>
										<div data-v-880f12fd="" className="bot_attr_dict"></div>
										<div data-v-880f12fd="" className="bot_attr_text">
											Wealth: {JSON.parse(data).Wealth} SAT
										</div>
									</div>
								</div>
							</div>
						</>
					)}
				</div>
				<Stack>
					<Typography
						className="gridprice"
						sx={{ fontSize: '23px', color: '#ebb94c' }}
					/>
				</Stack>
			</HomeItem>
			<Stack sx={{ bgcolor: '#1e1e1e', p: 2 }} className="boxbot">
				<Stack
					sx={{
						flexDirection: 'row',
						justifyContent: 'space-between',
					}}
				>
					<Stack
						sx={{
							color: 'grey',
							flexDirection: 'row',
							alignItems: 'center',
							my: 1,
						}}
					>
						<Typography
							sx={{
								display: 'flex',
								alignItems: 'center',
								flexGrow: 1,
								gap: 1,
								fontSize: '10px',
							}}
							component={'div'}
						>
							<img
								src="/images/bitcoin.png"
								alt=""
								style={{
									borderRadius: '50%',
									width: '20px',
									height: '20px',
								}}
							/>
							{bottomPrice.toFixed(8)}
						</Typography>
					</Stack>

					<Stack>
						<Typography
							sx={{
								fontSize: '10px',
								color: 'black',
								textAlign: 'left',
								paddingTop: '10px',
							}}
						>
							${usdAmount?.toFixed(5)}
						</Typography>
					</Stack>
				</Stack>

				<Button
					variant="outlined"
					sx={{ border: '1px solid grey', color: 'black !important' }}
					className="btn_buy"
					onClick={onBuyClick}
				>
					Buy
				</Button>
			</Stack>
		</>
	);
};

export default InscriptionGridContent;

import { FC } from 'react';
import { CircularProgress } from '@mui/material';
import useSWR from 'swr';
import OrdersTable from 'src/components/pages/home/OrdersTable';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import { OrderHistory } from 'src/utils/types';

const TabContentMyOrders: FC = () => {
	const { address, connected } = useAuthWallet();

	const {
		error,
		isLoading,
		data: myOrders = [],
	} = useSWR<OrderHistory[]>(() =>
		connected ? `/api/getMyOrders?addr=${address}` : null
	);

	if (!connected) {
		return <span style={{ color: 'red' }}>Connect to your wallet first !</span>;
	}

	if (isLoading || error || myOrders.length === 0) {
		return (
			<>
				{isLoading && (
					<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
				)}
				{!isLoading && error && (
					<span style={{ color: 'red' }}>{error.message}</span>
				)}
				{!isLoading && !error && myOrders.length === 0 && <span>No Item</span>}
			</>
		);
	}

	return <OrdersTable orders={myOrders} />;
};

export default TabContentMyOrders;

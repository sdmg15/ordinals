import { FC } from 'react';
import { CircularProgress } from '@mui/material';
import { OrderHistory } from 'src/utils/types';
import OrdersTable from 'src/components/pages/home/OrdersTable';
import useSWR from 'swr';

const TabContentOrders: FC = () => {
	const {
		error,
		isLoading,
		data: allOrders = [],
	} = useSWR<OrderHistory[]>('/api/getAllHistories');

	if (isLoading || error || allOrders.length === 0) {
		return (
			<>
				{isLoading && (
					<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
				)}
				{!isLoading && error && (
					<span style={{ color: 'red' }}>{error.message}</span>
				)}
				{!isLoading && !error && allOrders.length === 0 && <span>No Item</span>}
			</>
		);
	}

	return <OrdersTable orders={allOrders} />;
};

export default TabContentOrders;

import { FC } from 'react';

import { Button, Stack, Typography, CircularProgress } from '@mui/material';
import HomeItem from 'src/components/pages/home/HomeItem';
import { MyNFTInscription } from '.';
import { useGetInscriptionInfo } from 'src/hooks/useGetInscriptionInfo';

type NFTGridContentProps = {
	inscription: MyNFTInscription;
	onSellClick: () => void;
};

const NFTGridContent: FC<NFTGridContentProps> = ({
	inscription,
	onSellClick,
}) => {
	const { data: content, isLoading: inscriptionInfoLoading } =
		useGetInscriptionInfo(inscription.inscriptionId);

	return (
		<>
			<HomeItem className="boxtop">
				<Stack
					sx={{
						flexDirection: 'row',
						justifyContent: 'space-between',
					}}
				>
				</Stack>
				<Stack>
					<Typography sx={{ fontSize: '30px', color: '#ebb94c' }}>
						#{inscription.inscriptionNumber}
					</Typography>

					{inscriptionInfoLoading ? (
						<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
					) : (
						<Typography sx={{ fontSize: '10px' }}>
							{content}
						</Typography>
					)}
				</Stack>
				<Stack>
					<Typography
						className="gridprice"
						sx={{ fontSize: '23px', color: '#ebb94c' }}
					></Typography>
				</Stack>
			</HomeItem>
			<Stack sx={{ bgcolor: '#1e1e1e', p: 2 }} className="boxbot">
				<Stack>
					<Typography
						sx={{
							fontSize: '14px',
							color: '#ebb94c',
							textAlign: 'left',
							borderBottom: '1px solid #282828',
							pb: 2,
						}}
					>
						<a
							href={inscription.preview}
							target="_blank"
							style={{ marginLeft: '95px', color: 'white' }}
						>
							PREVIEW
						</a>
					</Typography>
				</Stack>

				<Button
					variant="outlined"
					sx={{ border: '1px solid grey', color: 'black !important'}}
					className="btn_buy"
					onClick={onSellClick}
				>
					Sell
				</Button>
			</Stack>
		</>
	);
};

export default NFTGridContent;

import { FC, useEffect, useState } from 'react';
import { Box, Grid, CircularProgress } from '@mui/material';
import toast from 'react-hot-toast';
import useAuthWallet from 'src/providers/AuthWalletProvider';
import HomeSellModal, {
	HomeSellModalProps,
} from 'src/components/modals/HomeSellModal';
import axios from 'axios';
import NFTGridContent from './NFTGridContent';

type TabContentMyNFTsProps = {
	isDarkMode: boolean;
};

const chunkSize = 20;

const TabContentMyNFTs: FC<TabContentMyNFTsProps> = ({ isDarkMode }) => {
	const [cursor] = useState(0);
	const [error, setError] = useState<string>('');
	const [modalError, setModalError] = useState<string>('');
	const [isLoading, setIsLoading] = useState(true);
	const [inscriptions, setInscriptions] = useState<InscriptionList>();
	const [selectedInscp, setSelectedInscp] = useState<MyNFTInscription | null>(
		null
	);
	const { connected } = useAuthWallet();

	useEffect(() => {

		fetchInscriptions();
	}, [connected]);

	const fetchData = async (inscr: MyNFTInscription) => {
		try {
			const response = await fetch('https://ordinals.com/content/'+ inscr.inscriptionId);

			if (response.ok) {
				const result = await response.json();

				if (result.Wisdom !== null) {
					return inscr;
				}					
			}
			return null;
		} catch (error) {
			setError(error.message);
			return null;
		}
	};

	const filterInscriptionsByFetchData = async (inscriptions: MyNFTInscription[]): 
	Promise<MyNFTInscription[]> => {
		try {
			const filteredInscriptions = await Promise.all(
				inscriptions.map(async (inscription) => {
					try {
						return await fetchData(inscription);
					} catch (error) {
						// Handle errors from fetchData if needed
						console.error(`Error fetching data for inscriptionId ${inscription.inscriptionId}: ${error.message}`);
						return null;
					}
				})
			);
	
			// Remove null values (inscriptions where fetchData returned null)
			return filteredInscriptions.filter((inscr) => inscr !== null) as MyNFTInscription[];
		} catch (error) {
			// Handle errors from the overall process if needed
			console.error(`Error filtering inscriptions: ${error.message}`);
			return [];
		}
	};

	const fetchInscriptions = async () => {
		const unisat = (window as any).unisat;

		setError('');
		setIsLoading(true);

		if (typeof unisat === 'undefined') {
			const errMsg = 'Unisat is not installed!';

			setError(errMsg);
			toast.error(errMsg);
			setIsLoading(false);
			return;
		}

		if (!connected) {
			const errMsg = 'Connect your wallet first !';

			setError(errMsg);
			toast.error(errMsg);
			setIsLoading(false);
			return;
		}

		try {
			const result = await (unisat.getInscriptions(
				cursor,
				chunkSize
			) as Promise<InscriptionList>);

			result.list = await filterInscriptionsByFetchData(result.list);
			result.total = result.list.length;
		
			setInscriptions(result);
		} catch (err: any) {
			setError(err.message);
		} finally {
			setIsLoading(false);
		}
	};

	const onSellSubmit: HomeSellModalProps['onSubmit'] = async (data) => {
		setModalError('');
		try {
			const result = await axios
				.post('/api/listInscriptionForSell', {
					price: data.price,
					sellerReceiveAddress: data.address,
					inscription: selectedInscp,
				})
				.then((res) => res.data);

			const signature = (await (window as any).unisat.signPsbt(
				result.seller.unsignedSellingPSBTHex,
				{ autoFinalized: true }
			)) as Promise<string>;

			result.seller.signedListingPSBTBase64 = signature;

			await axios.post('/api/postSellInscription', { ...result });

			fetchInscriptions();
			handleModalClose();
		} catch (error: any) {
			setModalError(error.message);
		}
	};

	const handleModalClose = () => {
		setSelectedInscp(null);
		setModalError('');
	};

	if (isLoading || error || inscriptions?.total === 0) {
		return (
			<>
				{isLoading && (
					<CircularProgress disableShrink style={{ color: '#1c1c1c' }} />
				)}
				{!isLoading && error && <span style={{ color: 'red' }}>{error}</span>}
				{!isLoading && !error && inscriptions?.total === 0 && (
					<span>No Item</span>
				)}
			</>
		);
	}

	return (
		<Box sx={{ flexGrow: 1 }}>
			<Grid
				container
				// spacing={{ xs: 2, md: 3 }}
				columns={{ xs: 4, sm: 8, md: 12 }}
				className="gridspace"
			>
				{inscriptions?.list.map((inscription, index) => (
					<Grid
						item
						xs={15}
						sm={4}
						md={4}
						key={index}
						className={`ss ${isDarkMode ? 'dark' : 'light'}`}
					>
						<NFTGridContent
							inscription={inscription}
							onSellClick={() => setSelectedInscp(inscription)}
						/>
					</Grid>
				))}
			</Grid>
			<HomeSellModal
				open={selectedInscp != null}
				onClose={handleModalClose}
				onSubmit={onSellSubmit}
				error={modalError}
			/>
		</Box>
	);
};

interface InscriptionList {
	list: MyNFTInscription[];
	total: number;
}

export interface MyNFTInscription {
	inscriptionId: string;
	inscriptionNumber: number;
	address: string;
	outputValue: number;
	preview: string;
	content: string;
	contentLength: number;
	contentType: string;
	contentBody: string;
	timestamp: number;
	genesisTransaction: string;
	location: string;
	output: string;
	offset: number;
	utxoHeight: number;
	utxoConfirmation: number;
}

export default TabContentMyNFTs;

import { FC, useState } from 'react';
import {
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TablePagination,
	TablePaginationOwnProps,
	TableRow,
} from '@mui/material';
import { OrderHistory } from 'src/utils/types';
import { shortenString } from 'src/utils/helpers';

type OrdersTableProps = {
	orders: OrderHistory[];
};

const OrdersTable: FC<OrdersTableProps> = ({ orders }) => {
	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);

	const handleChangePage: TablePaginationOwnProps['onPageChange'] = (
		_,
		newPage
	) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage: TablePaginationOwnProps['onRowsPerPageChange'] =
		(event) => {
			setRowsPerPage(+event.target.value);
			setPage(0);
		};

	return (
		<Paper sx={{ width: '100%', overflow: 'hidden' }}>
			<TableContainer sx={{ maxHeight: '640px' }}>
				<Table stickyHeader aria-label="sticky table">
					<TableHead>
						<TableRow>
							<TableCell>Inscription ID</TableCell>
							<TableCell>Event</TableCell>
							<TableCell>Price</TableCell>
							<TableCell>From</TableCell>
							<TableCell>To</TableCell>
							<TableCell>Created At</TableCell>
						</TableRow>
					</TableHead>
					<TableBody sx={{backgroundColor: 'white !important', color: 'black !important'}}>
						{orders
							.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
							.map((inscription, index) => {
								return (
									<TableRow hover role="checkbox" tabIndex={-1} key={index} sx={{color: 'black !important'}}>
										<TableCell>
											<a href={inscription.preview}>#{inscription.number}</a>
										</TableCell>
										<TableCell>{inscription.event}</TableCell>
										<TableCell>{inscription.price}</TableCell>
										<TableCell>{shortenString(inscription.from, 15)}</TableCell>
										<TableCell>{shortenString(inscription.to, 15)}</TableCell>
										<TableCell>
											{new Date(Number(inscription.createdAt)).toLocaleString(
												'en-US'
											)}
										</TableCell>
									</TableRow>
								);
							})}
					</TableBody>
				</Table>
			</TableContainer>
			<TablePagination
				rowsPerPageOptions={[10, 25, 100]}
				component="div"
				count={orders.length}
				rowsPerPage={rowsPerPage}
				page={page}
				onPageChange={handleChangePage}
				onRowsPerPageChange={handleChangeRowsPerPage}
			/>
		</Paper>
	);
};

export default OrdersTable;

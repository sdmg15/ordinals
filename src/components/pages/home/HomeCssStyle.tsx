import { FC } from 'react';

const HomeCssStyle: FC<{ isDarkMode: boolean }> = ({ isDarkMode }) => (
	<style jsx global>{`
		.gridbox {
			background: ${isDarkMode ? '#161616' : '#ffffff'} !important;
			color: ${isDarkMode ? 'white' : 'black'} !important;
		}

		.sidebtns {
			color: ${isDarkMode ? 'white' : 'black'} !important;
			background: ${isDarkMode ? '#161616' : '#ffffff'} !important;
		}
		.sidebtns:hover {
			background: #dbdbdb !important;
		}

		.App {
			background: ${isDarkMode ? '#161616' : '#ffffff'} !important;
		}

		.ss {
			background: #white !important;
		}

		.sidebtns.Mui-selected {
			color: ${isDarkMode ? 'white' : 'black'} !important;
			${isDarkMode ? '' : 'background: #dbdbdb !important;'}
		}

		.toggle-button {
			background-color: transparent;
			color: ${isDarkMode ? 'white' : 'black'} !important;
			border: none;
			padding: 10px;
			cursor: pointer;
			font-size: 18px;
			padding-left: 20px;
		}

		.logoapp {
			color: ${isDarkMode ? 'white' : 'black'} !important;
		}
		.connect-btn {
			background: ${isDarkMode ? 'white' : 'black'} !important;
			color: ${isDarkMode ? 'black' : 'white'} !important;
		}

		.MuiPagination-ul button {
			color: ${isDarkMode ? 'white' : 'black'} !important;
		}
	`}</style>
);

export default HomeCssStyle;

import { FC, HTMLAttributes, ReactNode } from 'react';
import { Box, Typography } from '@mui/material';

interface CustomTabPanelProps extends HTMLAttributes<HTMLDivElement> {
	children: ReactNode;
	index: number;
	value: number;
}

const CustomTabPanel: FC<CustomTabPanelProps> = ({
	children,
	value,
	index,
	...other
}) => {
	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box sx={{ p: 3 }}>
					<Typography component={'div'}>{children}</Typography>
				</Box>
			)}
		</div>
	);
};

export default CustomTabPanel;

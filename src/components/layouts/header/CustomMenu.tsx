import { Menu, MenuItem } from '@mui/material';
import { FC } from 'react';
import useAuthWallet from 'src/providers/AuthWalletProvider';

type CustomMenuProps = {
	anchorEl: Element | null;
	open: boolean;
	onClose: () => void;

	showAddress?: boolean;
	showBalance?: boolean;
};

const CustomMenu: FC<CustomMenuProps> = ({
	anchorEl,
	open,
	onClose,
	showAddress,
	showBalance,
}) => {
	const { address, balance, disconnectWallet } = useAuthWallet();

	return (
		<Menu
			id="basic-menu"
			anchorEl={anchorEl}
			open={open}
			onClose={onClose}
			MenuListProps={{ 'aria-labelledby': 'basic-button' }}
			PaperProps={{
				elevation: 0,
				sx: {
					overflow: 'visible',
					filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
					mt: 1.5,
					'& .MuiAvatar-root': {
						width: 32,
						height: 32,
						ml: -0.5,
						mr: 1,
					},
					'&:before': {
						content: '""',
						display: 'block',
						position: 'absolute',
						top: 0,
						right: 14,
						width: 10,
						height: 10,
						bgcolor: 'background.paper',
						transform: 'translateY(-50%) rotate(45deg)',
						zIndex: 0,
					},
				},
			}}
			transformOrigin={{ horizontal: 'right', vertical: 'top' }}
			anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
		>
			{showAddress && (
				<MenuItem onClick={onClose} sx={{ fontSize: '12px' }}>
					{address
						? `${address.slice(0, 6)}...${address.slice(
								address.length - 4,
								address.length
							)}`
						: ''}
				</MenuItem>
			)}
			{showBalance && (
				<MenuItem onClick={onClose}>{balance.total / 100000000}</MenuItem>
			)}
			<MenuItem onClick={disconnectWallet} sx={{ fontSize: '12px' }}>
				disconnect
			</MenuItem>
		</Menu>
	);
};

export default CustomMenu;

import {
	FC,
	PropsWithChildren,
	createContext,
	useContext,
	useEffect,
	useMemo,
	useState,
} from 'react';
import axios from 'axios';

const BitcoinRateContext = createContext<{ oneBtcInUsd?: number }>({});

export const BitcoinRateProvider: FC<PropsWithChildren> = ({ children }) => {
	const [bitcoinPriceData, setBitcoinPriceData] = useState<BitcoinPriceData>();

	useEffect(() => {
		axios
			.get<BitcoinPriceData>(
				'https://api.coindesk.com/v1/bpi/currentprice/USD.json'
			)
			.then((response) => response.data)
			.then((data) => {
				setBitcoinPriceData(data);
				// console.info(`Current Bitcoin Price: ${data.bpi.USD.rate}`);
			})
			.catch((error) => {
				console.error('Error fetching current Bitcoin price:', error);
			});
	}, []);

	const oneBtcInUsd = useMemo(
		() => bitcoinPriceData?.bpi.USD.rate_float,
		[bitcoinPriceData?.bpi.USD.rate_float]
	);

	return (
		<BitcoinRateContext.Provider value={{ oneBtcInUsd }}>
			{children}
		</BitcoinRateContext.Provider>
	);
};

export const useBitcoinRate = () => useContext(BitcoinRateContext);

interface BitcoinPriceData {
	time: {
		updated: string;
		updatedISO: string;
		updateduk: string;
	};
	disclaimer: string;
	bpi: {
		USD: {
			code: string;
			rate: string;
			description: string;
			rate_float: number;
		};
	};
}

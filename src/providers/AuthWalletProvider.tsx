import {
	FC,
	createContext,
	useContext,
	useEffect,
	useRef,
	useState,
} from 'react';
import toast from 'react-hot-toast';
import {
	GetAddressOptions,
	AddressPurpose,
	BitcoinNetworkType,
	getAddress,
} from 'sats-connect';
import ConnectWalletModal from 'src/components/modals/ConnectWalletModal';
import { UnisatBalance } from 'src/utils/types';

interface AuthWalletContextType {
	getBasicInfo: () => void;
	disconnectWallet: () => void;
	address: string | null;
	network: string | null;
	connected: boolean;
	publicKey: string;
	accounts: string[];
	balance: UnisatBalance;
	selectedwallet: string;

	connectModalOpened: boolean;
	setConnectModalOpened: (opened: boolean) => void;
}

const AuthWalletContext = createContext({} as AuthWalletContextType);

type AuthWalletProviderProps = {
	children: JSX.Element | JSX.Element[];
};

export const AuthWalletProvider: FC<AuthWalletProviderProps> = ({
	children,
}) => {
	const selfRef = useRef<{ accounts: string[] }>({
		accounts: [],
	});

	const [modalOpened, setModalOpened] = useState(false);

	const [address, setAddress] = useState<string | null>(null);

	const [network, setNetwork] = useState<string | null>(null);
	const [, setUnisatInstalled] = useState(false);
	const [connected, setConnected] = useState(false);
	const [publicKey, setPublicKey] = useState('');
	const [accounts, setAccounts] = useState<string[]>([]);
	const [balance, setBalance] = useState<UnisatBalance>({
		confirmed: 0,
		unconfirmed: 0,
		total: 0,
	});
	const [selectedwallet, setSelectedwallet] = useState('unisat');

	// -------------------------

	useEffect(() => {
		async function checkUnisat() {
			let unisat = (window as any).unisat;

			for (let i = 1; i < 10 && !unisat; i += 1) {
				if (unisat) break;
				await new Promise((resolve) => setTimeout(resolve, 100 * i));
				unisat = (window as any).unisat;
			}

			if (unisat) {
				setUnisatInstalled(true);
			} else if (!unisat) return;

			(unisat.getAccounts() as Promise<string[]>).then((accounts) => {
				handleAccountsChanged(accounts);
			});

			unisat.on('accountsChanged', handleAccountsChanged);
			unisat.on('networkChanged', handleNetworkChanged);

			return () => {
				unisat.removeListener('accountsChanged', handleAccountsChanged);
				unisat.removeListener('networkChanged', handleNetworkChanged);
			};
		}

		checkUnisat().then();
	}, []);

	const getBasicInfo = async () => {
		const unisat = (window as any).unisat;

		if (typeof unisat === 'undefined') {
			toast.error('Unisat is not installed!');
			return;
		}

		const [address] = await (unisat.getAccounts() as Promise<string[]>);

		setAddress(address);

		setPublicKey(await (unisat.getPublicKey() as Promise<string>));

		setBalance(await (unisat.getBalance() as Promise<UnisatBalance>));

		setNetwork(await (unisat.getNetwork() as Promise<string>));
	};

	const onUnisatWalletConnect = async () => {
		try {
			const unisat = (window as any).unisat;

			if (typeof unisat === 'undefined') {
				toast.error('Unisat is not installed!');
				return;
			}

			const result = await (unisat.requestAccounts() as Promise<string[]>);
			const balance = await (unisat.getBalance() as Promise<UnisatBalance>);

			setBalance(balance);
			handleAccountsChanged(result);
			setConnected(true);
			setModalOpened(false);
			setSelectedwallet('unisat');
		} catch (error: any) {
			toast.error(error.message);
		}
	};

	const onXverseWalletConnect = async () => {
		try {
			const getAddressOptions: GetAddressOptions = {
				payload: {
					purposes: [AddressPurpose.Payment],
					message: 'Address for receiving payments',
					network: {
						// type:  BitcoinNetworkType.Testnet,
						type: BitcoinNetworkType.Mainnet,
					},
				},
				onFinish: (response) => {
					// console.log(response);
					setAddress(response.addresses[0].address);
					setConnected(true);
					setModalOpened(false);
					setSelectedwallet('xverse');
				},
				onCancel: () => toast.error('Request canceled'),
			};

			await getAddress(getAddressOptions);
		} catch (error) {
			console.error(error);
		}
	};

	const onOkxWalletConnect = async () => {
		try {
			if (typeof (window as any).okxwallet === 'undefined') {
				toast.error('OKX is not installed!');
			} else {
				const result = await (window as any).okxwallet.bitcoin.connect();

				setAddress(result.address);
				setConnected(true);
				setModalOpened(false);
				setSelectedwallet('okx');
			}
		} catch (error: any) {
			toast.error(error.message);
		}
	};

	const onLeatherWalletConnect = async () => {
		try {
			if (typeof (window as any).btc === 'undefined') {
				toast.error('leather is not installed!');
			} else {
				const userAddresses = await (window as any).btc?.request(
					'getAddresses'
				);
				const usersNativeSegwitAddress = userAddresses.result.addresses.find(
					(address: any) => address.type === 'p2wpkh'
				);

				setAddress(usersNativeSegwitAddress.address);
				setConnected(true);
				setModalOpened(false);
				setSelectedwallet('leather');
			}
		} catch (error: any) {
			toast.error(error.message);
		}
	};

	const handleAccountsChanged = (_accounts: string[]) => {
		// if (self.accounts[0] === _accounts[0]) {
		//   // prevent from triggering twice
		//   setOpen(false);
		//   return;
		// }
		selfRef.current.accounts = _accounts;
		if (_accounts.length > 0) {
			setAccounts(_accounts);
			setConnected(true);

			setAddress(_accounts[0]);

			getBasicInfo();
		} else {
			setConnected(false);
		}
	};

	const handleNetworkChanged = (network: string) => {
		setNetwork(network);
		getBasicInfo();
	};

	const disconnectWallet = () => {
		setConnected(false);
		setBalance({
			confirmed: 0,
			unconfirmed: 0,
			total: 0,
		});
		setAccounts([]);
		setModalOpened(false);
	};

	const authWalletContextValue = {
		getBasicInfo,
		disconnectWallet,
		address,
		network,
		connected,
		publicKey,
		accounts,
		balance,
		selectedwallet,
		connectModalOpened: modalOpened,
		setConnectModalOpened: setModalOpened,
	};

	return (
		<AuthWalletContext.Provider value={authWalletContextValue}>
			{children}
			<ConnectWalletModal
				open={modalOpened}
				onClose={() => setModalOpened(false)}
				onUnisatWalletConnect={onUnisatWalletConnect}
				onXverseWalletConnect={onXverseWalletConnect}
				onOkxWalletConnect={onOkxWalletConnect}
				onLeatherWalletConnect={onLeatherWalletConnect}
			/>
		</AuthWalletContext.Provider>
	);
};

const useAuthWallet = () => useContext(AuthWalletContext);

export default useAuthWallet;

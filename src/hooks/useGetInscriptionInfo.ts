import useSWR from 'swr';

export const useGetInscriptionInfo = (inscriptionId: string) => {
	const { data, isLoading, error } = useSWR<string>(
		`/api/getInscriptionContent?inscriptionID=${inscriptionId}`,
		{
			revalidateOnMount: true, // no need to refetch when we change tab
			dedupingInterval: 15 * 60 * 1000, // do not make same request until 15 minutes are passed
		}
	);

	return { data, isLoading, error };
};

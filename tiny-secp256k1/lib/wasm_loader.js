import fs from 'fs';
import * as rand from './rand.js';
import * as validate_error from './validate_error.js';

const binary = fs.readFileSync(process.cwd() + '/secp256k1.wasm');

const mod = new WebAssembly.Module(binary);

const imports = {
  './rand.js': rand,
  './validate_error.js': validate_error,
};

const instance = new WebAssembly.Instance(mod, imports);

export default instance.exports;
